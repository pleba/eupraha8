<?php
  require('connect.php');
  $drazby_obsah = "";
  $popis_drazby_obsah = "";

  $sql = "SELECT drazby, popis_drazby_obsah FROM obsah WHERE id = '1'";
  $result = mysql_query($sql)
    or die("Invalid query: " . mysql_error($link));
  
  $row = mysql_fetch_array($result);    
  $drazby_obsah = $row['drazby'];     
  $popis_drazby_obsah = $row['popis_drazby_obsah']; 

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta name="keywords" content="<?php  echo strip_tags(stripslashes($popis_drazby_obsah)); ?>" />
<meta name="description" content="<?php  echo strip_tags(stripslashes($popis_drazby_obsah)); ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta name="robots" content="index,follow" />
<link href="styles/exekutor.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="exekutor.ico" type="image/x-icon" />
<title>Informace pro zájemce | Exekutorský Úřad Praha 8</title>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
</head>
<body>
<div style="display:none;">
  <img src="img/menu1_over.jpg" alt="">
  <img src="img/menu2_over.jpg" alt="">
  <img src="img/menu3_over.jpg" alt="">
  <img src="img/menu4_over.jpg" alt="">
  <img src="img/menu5_over.jpg" alt="">
  <img src="img/menu6_over.jpg" alt="">
  <img src="img/menu7_over.jpg" alt="">
  <img src="img/submenu1_over.jpg" alt="">
</div>
	<div id="back_center"><!--zacentrovane pozadi-->
		<div id="baner_main"></div><!--hlavni baner-->
		<div id="menu_main"><!--hlavni menu-->
			<ul class="tabs">
				<li><a href="index1.php" class="m1"><em>základní informace</em></a></li>
				<li><a href="pro-dluzniky.php" class="m2"><em>pro dlužníky</em></a></li>
				<li><a href="pro-zamestnavatele.php" class="m3"><em>pro zaměstnavatele</em></a></li>
				<li><a href="exekucni-cinnost.php" class="n4"><em>exekuční činnost</em></a></li>
				<li><a href="exekutorske-zapisy.php" class="m5"><em>exekutorské zápisy</em></a></li>
				<li><a href="drazby.php" class="m6"><em>dražby</em></a></li>
				<li><a href="kontakt.php" class="m7"><em>kontakt</em></a></li>
			</ul>
		</div>
		<div id="submenu">
			<div id="submenu_in">
				<ul class="tabs2">
				<li><a href="drazby.html" class="m"><em>přehled dražeb</em></a></li>
				<li><a href="informace_pro_zajemce.html" class="h"><em>informace pro zájemce</em></a></li>
				</ul>
			</div>
		</div>
		<div id="back_content1">
			<div id="content_top">	
				<div id="content_top_left">
					<img src="img/zakladni_informace.jpg" alt="Exekuční činnost" />
				</div>
				<h1 id="content_top_right1">Exekuční činnost</h1>
				<div id="content_top_right2"><div id="description"><?php  echo stripslashes($popis_drazby_obsah); ?></div></div>
			</div>
			<div id="back_content">
			  <div id="wysiwyg">
			  <?php echo $drazby_obsah; ?>
			  </div>
			</div>
		</div>
		<div id="footer1"></div>
		<div id="footer2">Copyright&copy;2008</div>
		<div id="footer3"><a href="http://www.artfocus.cz/tvorba-www-stranek/webdesign.html" target="_blank" class="copyrght" title="Tvorba www stránek - webdesign">webdesign</a>, <a href="http://www.artfocus.cz/seo-optimalizace/optimalizace-pro-vyhledavace.html" target="_blank" class="copyrght" title="SEO - optimalizace www stránek pro vyhledávače">SEO</a>: <a href="http://www.artfocus.cz" target="_blank" class="copyrght" title="Webdesign, SEO, grafické studio">ArtFocus</a></div>
	</div>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-2817892-15");
pageTracker._trackPageview();
</script>
</body>
</html>

