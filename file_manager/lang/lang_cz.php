<?php
$strings["curr_dir"] 		= "Aktuální adresář";
$strings["back"] 		= "zpět";
$strings["close"] 		= "zavřít";
$strings["create_dir"] 		= "Vytvoř adresář:";
$strings["create_dir_submit"] 	= "Vytvoř";
$strings["upload_file"] 	= "Nahraj soubor:";
$strings["upload_file_submit"] 	= "Nahraj";
$strings["sending"] 		= "Nahrávám ...";
$strings["title"] 		= "Manager souborů";
?>
