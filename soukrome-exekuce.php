<?php
  require('connect.php');
  $soukroma_exekuce = "";
  $popis_soukroma_exekuce = "";
  
  $sql = "SELECT soukrome_exekuce, popis_soukroma_exekuce FROM obsah WHERE id = '1'";
  $result = mysql_query($sql)
  or die("Invalid query: " . mysql_error($link));
  
  $row = mysql_fetch_array($result);    
  $soukroma_exekuce = stripslashes($row['soukrome_exekuce']);
  $popis_soukroma_exekuce = $row['popis_soukroma_exekuce'];

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta name="keywords" content="<?php  echo strip_tags(stripslashes($popis_soukroma_exekuce)); ?>" />
<meta name="description" content="<?php  echo strip_tags(stripslashes($popis_soukroma_exekuce)); ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta name="robots" content="index,follow" />
<link href="styles/exekutor.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="exekutor.ico" type="image/x-icon" />
<title>Soukromé exekuce | Exekutorský Úřad Praha 8</title>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
</head>
<body>
<div style="display:none;">  <img src="img/menu1_over.jpg" alt="" />
  <img src="img/menu2_over.jpg" alt="" />
  <img src="img/menu3_over.jpg" alt="" />
  <img src="img/menu4_over.jpg" alt="" />
  <img src="img/menu5_over.jpg" alt="" />
  <img src="img/menu6_over.jpg" alt="" />
  <img src="img/menu7_over.jpg" alt="" />
  <img src="img/submenu1_over.jpg" alt="" />
</div>
	<div id="back_center"><!--zacentrovane pozadi-->
		<a title="Exekutorský úřad Praha 8" href="index.html" id="odkaz_main"><span id="baner_main"></span></a><!--hlavni baner-->
		<div id="menu_main"><!--hlavni menu-->
			<ul class="tabs">
				<li><a href="soukrome-exekuce.html" class="n1"><em>základní informace</em></a></li>
				<li><a href="pro-dluzniky.html" class="m2"><em>pro dlužníky</em></a></li>
				<li><a href="pro-zamestnavatele.html" class="m3"><em>pro zaměstnavatele</em></a></li>
				<li><a href="exekucni-cinnost.html" class="m4"><em>exekuční činnost</em></a></li>
				<li><a href="exekutorske-zapisy.html" class="m5"><em>exekutorské zápisy</em></a></li>
				<li><a href="drazby.html" class="m6"><em>dražby</em></a></li>
				<li><a href="kontakt.html" class="m7"><em>kontakt</em></a></li>
			</ul>
		</div>
		<div id="submenu">
			<div id="submenu_in">
				<ul class="tabs2">
				<li><a href="soukrome-exekuce.html" class="n"><em>soukromé exekuce</em></a></li>
				<li><a href="uredni-deska.html" class="m"><em>úřední deska</em></a></li>
				<li><a href="pravni-uprava.html" class="m"><em>právní úprava</em></a></li>
				</ul>
			</div>
		</div>
		<div id="back_content1">
		  <div id="under_wysiwyg">
        <img id="left_picture" src="img/left_blue_soukrome_exe.jpg" alt="Soukromé exekuce" />
        <span class="new_line">&nbsp;</span>
        <img id="right_picture" src="img/right_soukrome_exe.jpg" alt="Soukromé exekuce" />			  
			  <div id="wysiwyg" style="clear: both; margin: 0px 20px;"><img id="empty_corner" src="img/empty_corner.gif" alt="Soukromé exekuce" />				
  			  <?php echo $soukroma_exekuce; ?>
			  </div>
			</div>		
		</div>
		<div id="footer1"></div>
		<div id="footer2">Copyright&copy;2008</div>
		<div id="footer3"><a href="http://www.artfocus.cz/tvorba-www-stranek/webdesign.html" target="_blank" class="copyrght" title="Tvorba www stránek - webdesign">webdesign</a>, <a href="http://www.artfocus.cz/seo-optimalizace/optimalizace-pro-vyhledavace.html" target="_blank" class="copyrght" title="SEO - optimalizace www stránek pro vyhledávače">SEO</a>: <a href="http://www.artfocus.cz" target="_blank" class="copyrght" title="Webdesign, SEO, grafické studio">ArtFocus</a></div>
	</div>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-2817892-15");
pageTracker._trackPageview();
</script>
</body>
</html>

