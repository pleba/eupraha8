<?php
  require('connect.php');
  $deska = "";

  
  $sql = "SELECT vyzvy FROM obsah2 WHERE id = '1'";
  $result = mysql_query($sql)
  or die("Invalid query: " . mysql_error($link));
  
  $row = mysql_fetch_array($result);    
  $deska = stripslashes($row['vyzvy']);
  
  
  if(isset($_GET['f']) && isset($_GET['r']))
  {
        
        switch ($_GET['f']) {
             case "1": $order = "znacka ";
               break;
             case "2": $order = "jmeno ";
               break;
             case "3": $order = "dat_zac ";
               break;
             default: $order = "dat_zac ";
               break;  
           } 
      
       switch ($_GET['r']) {
             case "1": $order .= "ASC";
               break;
             case "2": $order .= "DESC";
               break;
             
             default: $order = "DESC ";
               break;  
           }
  
  }
  
  else {
        $order = "dat_zac DESC";
       }
  
  $sql = "SELECT * FROM vyzvy WHERE id > 0 ORDER BY ".$order;
  $result_vyzvy = mysql_query($sql)
  or die("Invalid query: " . mysql_error($link));

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta name="keywords" content="vyzvy k vyzvednuti, výzvy, vyzvy" />
<meta name="description" content="Výzvy k vyzvednutí | Exekutorský Úřad Praha 8" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta name="robots" content="index,follow" />
<link href="styles/exekutor.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="exekutor.ico" type="image/x-icon" />
<title>Výzvy k vyzvednutí | Exekutorský Úřad Praha 8</title>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>

<!--[if IE 6]>
    <link href="styles/exekutor_ie6.css" rel="stylesheet" type="text/css" media="all"  /> 
    <![endif]-->
<!--[if IE 7]>
    <link href="styles/exekutor_ie6.css" rel="stylesheet" type="text/css" media="all"  /> 
    <![endif]-->


</head>
<body>
<div style="display:none;">  <img src="img/menu1_over.jpg" alt="" />
  <img src="img/menu2_over.jpg" alt="" />
  <img src="img/menu3_over.jpg" alt="" />
  <img src="img/menu4_over.jpg" alt="" />
  <img src="img/menu5_over.jpg" alt="" />
  <img src="img/menu6_over.jpg" alt="" />
  <img src="img/menu7_over.jpg" alt="" />
  <img src="img/submenu1_over.jpg" alt="" />
</div>
	<div id="back_center"><!--zacentrovane pozadi-->
		<a title="Exekutorský úřad Praha 8" href="index.html" id="odkaz_main"><span id="baner_main"></span></a><!--hlavni baner-->
		<div id="menu_main"><!--hlavni menu-->
			<ul class="tabs">
				<li><a href="soukrome-exekuce.html" class="n1"><em>základní informace</em></a></li>
				<li><a href="pro-dluzniky.html" class="m2"><em>pro dlužníky</em></a></li>
				<li><a href="pro-zamestnavatele.html" class="m3"><em>pro zaměstnavatele</em></a></li>
				<li><a href="exekucni-cinnost.html" class="m4"><em>exekuční činnost</em></a></li>
				<li><a href="exekutorske-zapisy.html" class="m5"><em>exekutorské zápisy</em></a></li>
				<li><a href="drazby.html" class="m6"><em>dražby</em></a></li>
				<li><a href="kontakt.html" class="m7"><em>kontakt</em></a></li>
			</ul>
		</div>
		<div id="submenu">
			<div id="submenu_in">
				<ul class="tabs2">
				<li><a href="soukrome-exekuce.html" class="m"><em>soukromé exekuce</em></a></li>
				<li><a href="uredni-deska.html" class="n"><em>úřední deska</em></a></li>
				<li><a href="pravni-uprava.html" class="m"><em>právní úprava</em></a></li>
				</ul>
			</div>
		</div>
		<div id="back_content1">
		  <div id="under_wysiwyg">
        <img id="left_picture2" src="img/left_blue_uredni_deska.jpg" alt="Úřední deska" />
        
            <div id="left_subsub_menu" >
      				<ul class="tabs3" >
      				<li><a href="nedorucene-pisemnosti.html" class="m"><em>&nbsp;</em></a></li>
      				<li><a href="uredni-deska.html" class="c"><em>&nbsp;</em></a></li>
      				<li><a href="drazby.html" class="d"><em>&nbsp;</em></a></li>
      				</ul>
      			</div>
      	<span class="new_line">&nbsp;</span>
        <img id="right_picture" src="img/right_soukrome_exe.jpg" alt="Soukromé exekuce" />
      <span class="new_line">&nbsp;</span>
        <div id="nadpis_blueback">VÝZVA K VYZVEDNUTÍ ZÁSILKY</div> 
			  <div id="wysiwyg" style="clear: both; margin: 0px 20px;">				
  			<?php echo $deska; ?>
  			</div>
  			
        <table  cellspacing="0" id="table_data_ned" >
  			<tr style="background-color:rgb(0,62,123); ">
            <th style="width: 144px;letter-spacing: -1px ;">SPISOVÁ ZNAČKA <a href="uredni-deska-F1-R1.html"><img src="img/hor_sipka.jpg" /></a><a href="uredni-deska-F1-R2.html"><img src="img/spod_sipka.jpg" /></a></th>
            <th style="width: 175px;letter-spacing: -1px ;">JMÉNO ADRESÁTA <a href="uredni-deska-F2-R1.html"><img src="img/hor_sipka.jpg" /></a><a href="uredni-deska-F2-R2.html"><img src="img/spod_sipka.jpg" /></a></th>
            <th style="width: 285px;letter-spacing: -1px ;">POPIS</th>
            <th style="width: 135px;letter-spacing: -1px ;">DATUM VYVĚŠENÍ <a href="uredni-deska-F3-R1.html"><img src="img/hor_sipka.jpg" /></a><a href="uredni-deska-F3-R2.html"><img src="img/spod_sipka.jpg" /></a></th>
            <th style="width: 135px;letter-spacing: -1px ;">DATUM SMAZÁNÍ</th>
        </tr>
  			
  			<?php 
        
         while ($row_l = mysql_fetch_array($result_vyzvy)) {
         
         echo "<tr><td >".$row_l['znacka']."</td>
               <td >".$row_l['jmeno']."</td>
               <td >".$row_l['popis']."</td>";
               
               $dat_zac = $row_l['dat_zac'];
                 $dat_zac_ex = explode("-",$dat_zac);
               $dat_kon = $row_l['dat_kon'];
                 $dat_kon_ex = explode("-",$dat_kon);
               
        echo  "<td >".$dat_zac_ex[2].".".$dat_zac_ex[1].".".$dat_zac_ex[0]."</td>
               <td >".$dat_kon_ex[2].".".$dat_kon_ex[1].".".$dat_kon_ex[0]."</td>              
              </tr>
              ";
         
         
         
         }
        
        
        ?>
  			
  			
        </table>
        
  			
			</div>		
		</div>
		<div id="footer1"></div>
		<div id="footer2">Copyright&copy;2008</div>
		<div id="footer3"><a href="http://www.artfocus.cz/tvorba-www-stranek/webdesign.html" target="_blank" class="copyrght" title="Tvorba www stránek - webdesign">webdesign</a>, <a href="http://www.artfocus.cz/seo-optimalizace/optimalizace-pro-vyhledavace.html" target="_blank" class="copyrght" title="SEO - optimalizace www stránek pro vyhledávače">SEO</a>: <a href="http://www.artfocus.cz" target="_blank" class="copyrght" title="Webdesign, SEO, grafické studio">ArtFocus</a></div>
	</div>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-2817892-15");
pageTracker._trackPageview();
</script>
</body>
</html>

