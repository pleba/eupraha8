<?php
  require('connect.php');
  require('functions.php');
  $popis_drazby_obsah = "";

  $sql = "SELECT popis_drazby_obsah FROM obsah WHERE id = '1'";
  $result = mysql_query($sql)
    or die("Invalid query: " . mysql_error($link));
  
  $row = mysql_fetch_array($result);   
  $popis_drazby_obsah = $row['popis_drazby_obsah']; 
      
  $sql_drazby = "SELECT id, nadpis, kratky_text, datum_a_cas, odhadovana_cena, vyvolavaci_cena, id_obrazek, stav FROM drazby WHERE cancel='0'";
  $result_drazby = mysql_query($sql_drazby)
    or die("Invalid query: " . mysql_error($result_drazby));
  $drazby_array = array();
  $i=0;
  while ($row_drazby = mysql_fetch_array($result_drazby)) {
    $drazby_array[$i]['id'] = $row_drazby['id'];
    $drazby_array[$i]['nadpis'] = stripslashes($row_drazby['nadpis']);
    $drazby_array[$i]['kratky_text'] = stripslashes($row_drazby['kratky_text']);
    $drazby_array[$i]['datum_a_cas'] = substr($row_drazby['datum_a_cas'],8 , 2) . "." . substr($row_drazby['datum_a_cas'],5 , 2) . "." . substr($row_drazby['datum_a_cas'],0 , 4) . substr($row_drazby['datum_a_cas'],10 , 6);
    $drazby_array[$i]['odhadovana_cena'] = $row_drazby['odhadovana_cena'];
    $drazby_array[$i]['vyvolavaci_cena'] = $row_drazby['vyvolavaci_cena'];
    $prepared_name = prepare_name($drazby_array[$i]['nadpis']);
    $drazby_array[$i]['odkaz'] = make_detail_url($drazby_array[$i]['id'], $prepared_name);
    $obrazek = $uploadmiddle . "obrazek_" . $row_drazby['id_obrazek'] . ".jpg";
    if (file_exists($obrazek)){
      $drazby_array[$i]['obrazek'] = $obrazek;
    } else {
      $drazby_array[$i]['obrazek'] = "img/bez_obrazku.jpg";
    }
    
    $drazby_array[$i]['stav'] = $row_drazby['stav'];
    $i++;
  }

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta name="keywords" content="<?php  echo strip_tags(stripslashes($popis_drazby_obsah)); ?>" />
<meta name="description" content="<?php  echo strip_tags(stripslashes($popis_drazby_obsah)); ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta name="robots" content="index,follow" />
<link href="styles/exekutor.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="exekutor.ico" type="image/x-icon" />
<title>Přehled dražeb | Exekutorský Úřad Praha 8</title>
<!--[if lt IE 7]>  
<script src="http://ie7-js.googlecode.com/svn/version/2.0(beta3)/IE7.js" type="text/javascript"></script>  
<![endif]--> 
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script> 
</head>
<body>
<div style="display:none;">  <img src="img/menu1_over.jpg" alt="" />
  <img src="img/menu2_over.jpg" alt="" />
  <img src="img/menu3_over.jpg" alt="" />
  <img src="img/menu4_over.jpg" alt="" />
  <img src="img/menu5_over.jpg" alt="" />
  <img src="img/menu6_over.jpg" alt="" />
  <img src="img/menu7_over.jpg" alt="" />
  <img src="img/submenu1_over.jpg" alt="" />
</div>
	<div id="back_center"><!--zacentrovane pozadi-->
		<a title="Exekutorský úřad Praha 8" href="index.html" id="odkaz_main"><span id="baner_main"></span></a><!--hlavni baner-->
		<div id="menu_main"><!--hlavni menu-->
			<ul class="tabs">
				<li><a href="soukrome-exekuce.html" class="m1"><em>základní informace</em></a></li>
				<li><a href="pro-dluzniky.html" class="m2"><em>pro dlužníky</em></a></li>
				<li><a href="pro-zamestnavatele.html" class="m3"><em>pro zaměstnavatele</em></a></li>
				<li><a href="exekucni-cinnost.html" class="m4"><em>exekuční činnost</em></a></li>
				<li><a href="exekutorske-zapisy.html" class="m5"><em>exekutorské zápisy</em></a></li>
				<li><a href="drazby.html" class="n6"><em>dražby</em></a></li>
				<li><a href="kontakt.html" class="m7"><em>kontakt</em></a></li>
			</ul>
		</div>
		<div id="submenu">
			<div id="submenu_in">
				<ul class="tabs2">
				<li><a href="drazby.html" class="n"><em>přehled dražeb</em></a></li>
				<li><a href="informace-pro-zajemce.html" class="g"><em>informace pro zájemce</em></a></li>
				</ul>
			</div>
		</div>
		<div id="back_content1">
			<div id="content_top" style="background:url(img/left_blue_drazby.jpg) no-repeat; ">	
				<div id="content_top_left">
					<img src="img/drazby.jpg" alt="Přehled dražeb" />
				</div>
				<div id="content_top_right2"><div id="description"><?php  echo stripslashes($popis_drazby_obsah); ?></div></div>
			</div><div id="back_content_seznam"><?php 
			  if (count($drazby_array) > 0) {
          for($i=0; $i<count($drazby_array); $i++) {
            echo "<div class=\"wraper_list_drazby\"><table cellpadding=\"2\" cellspacing=\"0\" class=\"table_list_drazby\">
              <tr>
                <td rowspan=\"3\" class=\"td_list_picture\"><a href=\"{$drazby_array[$i]['odkaz']}\" title=\"{$drazby_array[$i]['nadpis']}\" class=\"nadpisodkaz\"><img src=\"{$drazby_array[$i]['obrazek']}\" title=\"{$drazby_array[$i]['nadpis']}\" alt=\"{$drazby_array[$i]['nadpis']}\" class=\"list_picture\" /></a>
                </td>
                <td rowspan=\"3\" class=\"td_list_main\" align=\"left\">
                  <strong class=\"list_nadpis\"><a href=\"{$drazby_array[$i]['odkaz']}\" title=\"{$drazby_array[$i]['nadpis']}\" class=\"nadpisodkaz\">{$drazby_array[$i]['nadpis']}</a></strong>
                  <div class=\"new_line\"></div>
                  <span class=\"informace_list\">informace:</span>
                  <div class=\"new_line\"></div>
                  <span class=\"kratky_text_list\">{$drazby_array[$i]['kratky_text']}</span>
                </td>
                <td valign=\"bottom\" class=\"pevny_text_dc\">datum a čas</td>
                <td valign=\"bottom\" class=\"prom_text_dc\">{$drazby_array[$i]['datum_a_cas']}</td>
              </tr>
              <tr>
                <td valign=\"bottom\" class=\"pevny_text_dc\">odhadní cena</td>
                <td valign=\"bottom\" class=\"prom_text_dc\">" . number_format($drazby_array[$i]['odhadovana_cena'], 0, ',', ' ') . "&nbsp;Kč</td>
              </tr>
              <tr>              
                <td valign=\"bottom\" class=\"pevny_text_dc\" style=\"border-bottom: 0px;\">nejnižší podání</td>
                <td valign=\"bottom\" class=\"prom_text_dc\" style=\"border: 0px;\">" . number_format($drazby_array[$i]['vyvolavaci_cena'], 0, ',', ' ') . "&nbsp;Kč</td>
              </tr>
            </table>";
            if ($drazby_array[$i]['stav'] == 1) {
              echo "<img src=\"img/zrusena-trans.png\" title=\"Tato dražba byla zrušena\" alt=\"Tato dražba byla zrušena\" class=\"list_zruseno\" style=\"z-index: 10000; display: block;\" />";
            }
            echo "</div>          
            <span class=\"new_line\"></span>";
          }
        } elseif (count($drazby_array) == 0) {
          echo "<div class=\"wraper_list_drazby\"><table cellpadding=\"2\" cellspacing=\"0\" class=\"table_list_drazby\">
              <tr>
                <td align=\"center\" valign=\"middle\">
                V současné době nejsou nařízeny žádné dražby. 
                </td>                
              </tr>
            </table>";
        }
      ?>
			</div>
		</div>
		<div id="footer1"></div>
		<div id="footer2">Copyright&copy;2008</div>
		<div id="footer3"><a href="http://www.artfocus.cz/tvorba-www-stranek/webdesign.html" target="_blank" class="copyrght" title="Tvorba www stránek - webdesign">webdesign</a>, <a href="http://www.artfocus.cz/seo-optimalizace/optimalizace-pro-vyhledavace.html" target="_blank" class="copyrght" title="SEO - optimalizace www stránek pro vyhledávače">SEO</a>: <a href="http://www.artfocus.cz" target="_blank" class="copyrght" title="Webdesign, SEO, grafické studio">ArtFocus</a></div>
	</div>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-2817892-15");
pageTracker._trackPageview();
</script>
</body>
</html>
