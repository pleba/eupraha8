<?php
include "auth_user.inc.php";
if ($p_novy == 0) {
  //filtrování nadpisu
  if (isset($_SESSION['s_nadpis'])) {
    if ($_SESSION['s_nadpis'] != "") {
      $filtr_nadpis = " AND nadpis LIKE '" . $_SESSION['s_nadpis'] . "%'";
    } else { $filtr_nadpis = "";}
  } else { $filtr_nadpis = "";}

  //filtrování od kdy
  if (isset($_SESSION['s_odkdy'])) {
    if ($_SESSION['s_odkdy'] != "") {
      $od_kdy_sestav = substr($_SESSION['s_odkdy'],6 , 4) . "-" . substr($_SESSION['s_odkdy'],3 , 2) . "-" . substr($_SESSION['s_odkdy'],0 , 2);
      $filtr_odkdy = " AND datum_a_cas > '" . $od_kdy_sestav . "'";
    } else { $filtr_odkdy = "";}
  } else { $filtr_odkdy = "";}

  //filtrování do kdy
  if (isset($_SESSION['s_dokdy'])) {
    if ($_SESSION['s_dokdy'] != "") {
      $do_kdy_sestav = substr($_SESSION['s_dokdy'],6 , 4) . "-" . substr($_SESSION['s_dokdy'],3 , 2) . "-" . substr($_SESSION['s_dokdy'],0 , 2);
      $filtr_dokdy = " AND datum_a_cas < '" . $do_kdy_sestav . "'";
    } else { $filtr_dokdy = "";}
  } else { $filtr_dokdy = "";}

  //filtrování podle odhadované ceny
  if (isset($_SESSION['s_ocena'])) {
    if ($_SESSION['s_ocena'] != "") {
      switch ($_SESSION['s_ocena']) {
        case "49999": $filtr_ocena = " AND odhadovana_cena < 50000";
        break;
        case "99999": $filtr_ocena = " AND ((odhadovana_cena >= 50000) AND (odhadovana_cena < 100000))";
        break;
        case "499999": $filtr_ocena = " AND ((odhadovana_cena >= 100000) AND (odhadovana_cena < 500000))";
        break;
        case "999999": $filtr_ocena = " AND ((odhadovana_cena >= 500000) AND (odhadovana_cena < 1000000))";
        break;
        case "1000000": $filtr_ocena = " AND odhadovana_cena >= 1000000";
        break;
        default: $filtr_ocena = "";
        break;
      }
    } else { $filtr_ocena = "";}
  } else { $filtr_ocena = "";}

  //filtrování podle vyvolávací ceny
  if (isset($_SESSION['s_vcena'])) {
    if ($_SESSION['s_vcena'] != "") {
      switch ($_SESSION['s_vcena']) {
        case "49999": $filtr_vcena = " AND vyvolavaci_cena < 50000";
        break;
        case "99999": $filtr_vcena = " AND ((vyvolavaci_cena >= 50000) AND (vyvolavaci_cena < 100000))";
        break;
        case "499999": $filtr_vcena = " AND ((vyvolavaci_cena >= 100000) AND (vyvolavaci_cena < 500000))";
        break;
        case "999999": $filtr_vcena = " AND ((vyvolavaci_cena >= 500000) AND (vyvolavaci_cena < 1000000))";
        break;
        case "1000000": $filtr_vcena = " AND vyvolavaci_cena >= 1000000";
        break;
        default: $filtr_vcena = "";
        break;
      }
    } else { $filtr_vcena = "";}
  } else { $filtr_vcena = "";}

  //filtrování podle platnosti dražby
  if (isset($_SESSION['s_is_valid'])) {
    switch ($_SESSION['s_is_valid']) {
      case "aktivní": $filtr_validity = " AND stav = 0";
        break;
      case "zrušené": $filtr_validity = " AND stav = 1";
        break;
      case "": $filtr_validity = "";
        break;
      default: $filtr_validity = " AND stav = 0";
        break;
    }
  } else { $filtr_validity = " AND stav = 0";}

  //filtrování podle smazanosti dražby
  if (isset($_SESSION['s_is_deleted'])) {
    switch ($_SESSION['s_is_deleted']) {
      case "aktivní": $filtr_deleted = " AND cancel = 0";
        break;
      case "smazané": $filtr_deleted = " AND cancel = 1";
        break;
      case "": $filtr_deleted = "";
        break;
      default: $filtr_deleted = " AND cancel = 0";
        break;
    }
  } else { $filtr_deleted = " AND cancel = 0";}

  //řazení
  if ((isset($_SESSION['s_order'])) and (isset($_SESSION['s_how']))) {
   switch ($_SESSION['s_order']) {
     case "nadpis": $order_drazby = "nadpis ";
       break;
     case "datum_a_cas": $order_drazby = "datum_a_cas ";
       break;
     case "odhadovana_cena": $order_drazby = "odhadovana_cena ";
       break;
     case "vyvolavaci_cena": $order_drazby = "vyvolavaci_cena ";
       break;
     default: $order_drazby = "datum_a_cas ";
       break;
   }

   switch ($_SESSION['s_how']) {
     case "down": $order_drazby .= "DESC";
       break;
     case "up": $order_drazby .= "ASC";
       break;
     default: $order_drazby .= "ASC";
       break;
   }
  } else { $order_drazby="datum_a_cas ASC";}

  $sql_l = "SELECT id, nadpis, datum_a_cas, odhadovana_cena, vyvolavaci_cena, stav, cancel  FROM drazby WHERE id>0" . $filtr_nadpis . $filtr_odkdy . $filtr_dokdy . $filtr_ocena . $filtr_vcena . $filtr_validity . $filtr_deleted . " ORDER BY " . $order_drazby;

  $result_l = mysql_query($sql_l, $link)
      or die(mysql_error($link));

  $order_array = array();
  while ($row_l = mysql_fetch_array($result_l)) {
    $order_array[] = $row_l['id'];
  }

  $id_position = array_search($p_id, $order_array);
  if ($id_position == 0) {
    $left_id = 0;
    if ($id_position == (count($order_array)-1)) {
      $right_id = 0;
    } else {
      $right_id = $order_array[$id_position+1];
    }
  }
  elseif ($id_position == (count($order_array)-1)) {
    $left_id = $order_array[$id_position-1];
    $right_id = 0;
  } else {
    $left_id = $order_array[$id_position-1];
    $right_id = $order_array[$id_position+1];
  }
}

$p_odkaz_vyhlaska = $uploadvyhlaska . "vyhlaska_" . $p_id;
if (file_exists($p_odkaz_vyhlaska . ".doc")) {
  $p_odkaz_vyhlaska = $p_odkaz_vyhlaska . ".doc";
} elseif (file_exists($p_odkaz_vyhlaska . ".pdf")) {
  $p_odkaz_vyhlaska = $p_odkaz_vyhlaska . ".pdf";
}

$p_odkaz_posudek = $uploadposudek . "posudek_" . $p_id;
if (file_exists($p_odkaz_posudek . ".doc")) {
  $p_odkaz_posudek = $p_odkaz_posudek . ".doc";
} elseif (file_exists($p_odkaz_posudek . ".pdf")) {
  $p_odkaz_posudek = $p_odkaz_posudek . ".pdf";
}

$p_odkaz_usneseni = $uploadusneseni . "usneseni_" . $p_id;
if (file_exists($p_odkaz_usneseni . ".doc")) {
  $p_odkaz_usneseni = $p_odkaz_usneseni . ".doc";
} elseif (file_exists($p_odkaz_usneseni . ".pdf")) {
  $p_odkaz_usneseni = $p_odkaz_usneseni . ".pdf";
}

$p_odkaz_oznameni = $uploadoznameni . "oznameni_" . $p_id;
if (file_exists($p_odkaz_oznameni . ".doc")) {
    $p_odkaz_oznameni = $p_odkaz_oznameni . ".doc";
} elseif (file_exists($p_odkaz_oznameni . ".pdf")) {
    $p_odkaz_oznameni = $p_odkaz_oznameni . ".pdf";
}

if (file_exists($p_odkaz_vyhlaska)){
  $stav_vyhlaska = "<a href=\"$p_odkaz_vyhlaska\" title=\"Náhled\" target=\"_blank\" style=\"background: transparent;\">soubor nahrán, název: " . $p_odkaz_vyhlaska . ", čas: " . date ("j.m.Y, H:i:s", filemtime($p_odkaz_vyhlaska)) . ", velikost: " . number_format((filesize($p_odkaz_vyhlaska)/1024), 2, ',', ' ') . "kB</a>";
} else {
  $stav_vyhlaska = "soubor nebyl nahrán";
}

if (file_exists($p_odkaz_posudek)){
  $stav_posudek = "<a href=\"$p_odkaz_posudek\" title=\"Náhled\" target=\"_blank\" style=\"background: transparent;\">soubor nahrán, název: " . $p_odkaz_posudek . ", čas: " . date ("j.m.Y, H:i:s", filemtime($p_odkaz_posudek)) . ", velikost: " . number_format((filesize($p_odkaz_posudek)/1024), 2, ',', ' ') . "kB</a>";
} else {
  $stav_posudek = "soubor nebyl nahrán";
}

if (file_exists($p_odkaz_usneseni)){
  $stav_usneseni = "<a href=\"$p_odkaz_usneseni\" title=\"Náhled\" target=\"_blank\" style=\"background: transparent;\">soubor nahrán, název: " . $p_odkaz_usneseni . ", čas: " . date ("j.m.Y, H:i:s", filemtime($p_odkaz_usneseni)) . ", velikost: " . number_format((filesize($p_odkaz_usneseni)/1024), 2, ',', ' ') . "kB</a>";
} else {
  $stav_usneseni = "soubor nebyl nahrán";
}

if (file_exists($p_odkaz_oznameni)){
    $stav_oznameni = "<a href=\"$p_odkaz_oznameni\" title=\"Náhled\" target=\"_blank\" style=\"background: transparent;\">soubor nahrán, název: " . $p_odkaz_oznameni . ", čas: " . date ("j.m.Y, H:i:s", filemtime($p_odkaz_oznameni)) . ", velikost: " . number_format((filesize($p_odkaz_oznameni)/1024), 2, ',', ' ') . "kB</a>";
} else {
    $stav_oznameni = "soubor nebyl nahrán";
}

echo "
<script type=\"text/javascript\">
  var ClickBack = \"\";
  var ClickDelete = \"\";
  var blocking = \"0\";

  var sdiak = \"áäčďéěíĺľňóôöŕšťúůüýřžÁÄČĎÉĚÍĹĽŇÓÔÖŔŠŤÚŮÜÝŘŽ\";
  var bdiak = \"aacdeeillnooorstuuuyrzAACDEEILLNOOORSTUUUYRZ\";

  function bezdiak(in_value)
  {
    var tx = \"\";
    var je_diakr = 0;
    for(var p = 0; p < in_value.length; p++)
    {
      if (sdiak.indexOf(in_value.charAt(p)) != -1)
      {
        tx += bdiak.charAt(sdiak.indexOf(in_value.charAt(p)));
        je_diakr = 1;
      }
      else tx += in_value.charAt(p);
    }
    return je_diakr;
  }

  function submitform() {
     window.alert(\"jsem tu\");
     document.getElementById('id_input_platba').value = 'platba';
     document.mainform.submit();
  }

  function checkform(f) {
    var errortext = \"\";
    var datum_f = /^[0-9]{2}\.[0-9]{2}\.[0-9]{4}\ [0-9]{2}\:[0-9]{2}\$/;
    var price_f = /^\\d{1,10}.?\\d{0,2}\$/;

    if (ClickBack == \"back\") {
      return true;
    } else if (ClickDelete == \"delete\") {
      if (confirm(\"Skutečně chcete smazat tuto objednávku?\")) {
        return true;
      } else { return false;}
    } else {
      if (f.elements[\"nadpis\"].value == \"\") errortext += \"nadpis musí být zadán\" + \"\\n\";
      if (f.elements[\"datum\"].value == \"\") {
        errortext += \"datum musí být zadáno\" + \"\\n\";
      } else {
        if (!datum_f.test(f.elements[\"datum\"].value)) errortext += \"datum musí být uveden ve tvaru 26.01.2008 10:00\" + \"\\n\";
      }
      if (f.elements[\"ocena\"].value == \"\") {
        errortext += \"odhadovaná cena musí být zadána\" + \"\\n\";
      } else {
        f.elements[\"ocena\"].value=f.elements[\"ocena\"].value.replace(/,/, \".\")
        if (!price_f.test(f.elements[\"ocena\"].value)) errortext += \"odhadovaná cena musí být ve tvaru nnnnnnnnnn,nn\" + \"\\n\";
      }

      if (bezdiak(f.elements[\"vposudek\"].value) == 1) {
        errortext += \"název souboru popisující posudek nesmí obsahovat diakritiku\" + \"\\n\";
      }

      if (bezdiak(f.elements[\"vvyhlaska\"].value) == 1) {
        errortext += \"název souboru popisující dražební vyhlášku nesmí obsahovat diakritiku\" + \"\\n\";
      }

      if (bezdiak(f.elements[\"vpriklep\"].value) == 1) {
        errortext += \"název souboru popisující usnesení o příklepu nesmí obsahovat diakritiku\" + \"\\n\";
      }

      if (f.elements[\"vcena\"].value == \"\") {
        errortext += \"vyvolávací cena musí být zadána\" + \"\\n\";
      } else {
        f.elements[\"vcena\"].value=f.elements[\"vcena\"].value.replace(/,/, \".\")
        if (!price_f.test(f.elements[\"vcena\"].value)) errortext += \"vyvolávací cena musí být ve tvaru nnnnnnnnnn,nn\" + \"\\n\";
      }

      if (f.elements[\"kratky_text\"].value == \"\") errortext += \"krátký popis musí být zadán\" + \"\\n\";

      if(errortext!=\"\"){
        window.alert(errortext);
        return false;
      } else {
        return true;
      }
    }
  }

  function fback() { ClickBack = \"back\";}

  function fdelete() { ClickDelete = \"delete\";}

</script>";
    if ($p_novy == 0) {
      echo "<div style=\"position: relative; float: left; margin: 5px 20px 0px 20px; color: grey; font-size: 10px;\">FILTR: nadpis "; if (isset($_SESSION['s_nadpis'])) {if ($_SESSION['s_nadpis'] != "") {echo $_SESSION['s_nadpis'];} else { echo "není";} } else { echo "není";}
      echo "; od kdy "; if (isset($_SESSION['s_odkdy'])) {if ($_SESSION['s_odkdy'] != "") {echo $_SESSION['s_odkdy'];} else { echo "není";} } else { echo "není";}
      echo "; do kdy "; if (isset($_SESSION['s_dokdy'])) {if ($_SESSION['s_dokdy'] != "") {echo $_SESSION['s_dokdy'];} else { echo "není";} } else { echo "není";}
      echo "; odhadovaná cena ";
      if (isset($_SESSION['s_ocena'])) {
        if ($_SESSION['s_ocena'] != "") {
          switch ($_SESSION['s_ocena']) {
            case 49999: echo "0 - 49 999 Kč";
            break;
            case 99999: echo "500 - 99 999 Kč";
            break;
            case 499999: echo "100 000 - 499 999 Kč";
            break;
            case 999999: echo "500 000 - 999 999 Kč";
            break;
            case 1000000: echo "1 000 000 Kč a více";
            break;
          }
        } else { echo "není";}
      } else { echo "není";}
      echo "; vyvolávací cena ";
      if (isset($_SESSION['s_vcena'])) {
        if ($_SESSION['s_vcena'] != "") {
          switch ($_SESSION['s_vcena']) {
            case 49999: echo "0 - 49 999 Kč";
            break;
            case 99999: echo "500 - 99 999 Kč";
            break;
            case 499999: echo "100 000 - 499 999 Kč";
            break;
            case 999999: echo "500 000 - 999 999 Kč";
            break;
            case 1000000: echo "1 000 000 Kč a více";
            break;
          }
        } else { echo "není";}
      } else { echo "není";}
      echo "; platnost dražby "; if (isset($_SESSION['s_is_valid'])) {if ($_SESSION['s_is_valid'] != "") {echo $_SESSION['s_is_valid'];} else { echo "není";} } else { echo "není";}
      echo "; dražby vymazána "; if (isset($_SESSION['s_is_deleted'])) {if ($_SESSION['s_is_deleted'] != "") {echo $_SESSION['s_is_deleted'];} else { echo "není";} } else { echo "není";}
      echo "; <br />TŘÍDĚNÍ: podle ";
      if (isset($_SESSION['s_order'])) {
       switch ($_SESSION['s_order']) {
         case "nadpis":
           echo "nadpisu ";
           break;
         case "datum_a_cas";
           echo "datumu ";
           break;
         case "odhadovana_cena";
           echo "odhadované ceny ";
           break;
         case "vyvolavaci_cena";
           echo "vyvolávací ceny ";
           break;
         default:
           echo "datumu ";
           break;
       }
      }
      if (isset($_SESSION['s_how'])) {
       switch ($_SESSION['s_how']) {
         case "down":
           echo "sestupně";
           break;
         case "up";
           echo "vzestupně";
           break;
         default:
           echo "vzestupně";
           break;
       }
      }
      echo "</div>";
      if ($p_obrazek == 0) {
        echo "<div style=\"background-color: #FFD0F0; position: relative; float: left; margin: 10px 0px 0px 20px; height: 16px; width: 300px; border: outset; text-align: center; padding: 4px 0px; font-size: 12px;\">Žádný obrázek není zobrazen jako hlavní</div>";
      }

      echo "<div style=\"position: relative; float: right; margin: 10px 10px 0px 5px;\">
      <input type=\"button\" value=\"<< předchozí dražba\" name=\"submit\" style=\"width: 180px;\" ";
        if ($left_id == 0) { echo "disabled";}
        echo " onclick=\"location.href('index.php?type=drazby&amp;id_drazby=" . $left_id . "');\" />
      <input type=\"button\" value=\"následující dražba >>\" name=\"submit\" style=\"width: 180px;\" ";
        if ($right_id == 0) { echo "disabled";}
        echo " onclick=\"location.href('index.php?type=drazby&amp;id_drazby=" . $right_id . "');\" />
      </div>
      <div style=\"position: relative; clear: both; margin: 0px; border: 0px; padding: 0px; height: 0px;\"></div>";
    }
      echo "<form action=\"index.php?type=drazby\" method=\"post\" enctype=\"multipart/form-data\" onsubmit=\"return checkform(this);\" name=\"formdrazby\">
      <input type=\"hidden\" name=\"je_novy\" value=\"";
      if ($p_novy == 1) { echo "1"; } else { echo "0"; }
      echo "\" />

      <div style=\"position: relative; clear: both; height: auto !important; height: 500px; min-height: 500px; margin: 10px 0px 0px 20px;\">";
      if ($p_novy == 0) {
        echo "<div style=\"position: relative; float: left; margin: 0px 0px 0px 0px; width: 430px;\">Náhled detailu dražby s i. č. <span style=\"font-weight: 600; position: absolute; left: 245px;\">";
        if (isset($p_id)) {echo $p_id;}
        echo "</span></div>
        <input type=\"hidden\" name=\"id_drazby\" value=\"$p_id\" />";
      } else {
        echo "<div style=\"position: relative; float: left; margin: 0px 0px 30px 0px; width: 430px;\">Právě vkládáte novou dražbu</div>";
      }
      echo "<input type=\"submit\" name=\"zpet\" value=\"Zpět\" style=\"position: relative; float: right; width: 109px; margin: 0px 20px 0px 10px;\" onclick=\"fback();\" />";
      if ($p_novy == 0) {
        if ($p_cancel == 1) {
          echo "<input type=\"submit\" name=\"obnovit\" value=\"Obnovit\" style=\"position: relative; float: right; width: 109px; margin: 0px 10px 0px 10px;\" />";
        } elseif ($p_cancel == 0) {
          echo "<input type=\"submit\" name=\"smazat\" value=\"Vymazat\" style=\"position: relative; float: right; width: 109px; margin: 0px 10px 0px 10px;\" onclick=\"fdelete();\" />";
        }
      }
      echo "<input type=\"submit\" name=\"ulozit\" value=\"Uložit\" style=\"position: relative; float: right; width: 109px; margin: 0px 10px 0px 0px;\" />

      <div style=\"position: relative; clear: both; margin: 0px; border: 0px; padding: 0px; height: 0px;\"></div>
      <label title=\"Zvolte, zda má být dražba zrušena\" style=\"position: relative; float: left; font-size: 14px; font-weight: 200; margin: 3px 0px 20px 0px;\" for=\"id_zrusena\">Označte zda je dražba zrušena: </label><input title=\"Zvolte, zda má být dražba zrušena\" type=\"checkbox\" id=\"id_zrusena\" name=\"zrusena\" tabindex=\"1\" style=\"position: relative; float: left; font-size: 14px; font-weight: 600; margin: 0px 0px 0px 10px;\" ";
        if (isset($p_zruseno)) { if ($p_zruseno == 1) { echo "checked=\"checked\""; }}
      echo " />
      <div class=\"new_line_customer_up\"></div>
      <label title=\"Zadejte nadpis dražby\" class=\"editlabel\" for=\"id_nadpis\">Nadpis: <input title=\"Zadejte nadpis dražby\" type=\"text\" id=\"id_nadpis\" name=\"nadpis\" tabindex=\"2\" class=\"editfields\" value=\"";
      if (isset($p_nadpis)) {echo $p_nadpis;}
      echo "\" size=\"40\" maxlength=\"50\" /></label>
      <div class=\"new_line_customer_up\"></div>
      <label  title=\"Zadejte Datum a čas exekuce ve tvaru 11.10.2008 10:00\" class=\"editlabel\" for=\"id_datum\">Datum a čas: <input type=\"text\" id=\"id_datum\" name=\"datum\" tabindex=\"3\" class=\"editfields\" value=\"";
      if (isset($p_datum_a_cas)) {echo $p_datum_a_cas;}
      echo "\" size=\"16\" maxlength=\"16\" title=\"Zadejte Datum a čas exekuce ve tvaru 11.10.2008 10:00\" /></label>
      <div class=\"new_line_customer_up\"></div>
      <label  title=\"Zadejte Odhadovanou cenu\" class=\"editlabel\" for=\"id_ocena\">Odhadovaná cena: <input type=\"text\" id=\"id_ocena\" name=\"ocena\" tabindex=\"4\" class=\"editfields\" value=\"";
      if (isset($p_ocena)) {echo $p_ocena;}
      echo "\" size=\"10\" maxlength=\"10\" title=\"Zadejte Odhadovanou cenu\" /><span style=\"position: relative; left: 79px; top: -2px;\">Kč</span></label>
      <div class=\"new_line_customer_up\"></div>
      <label  title=\"Zadejte Vyvolávací cenu\" class=\"editlabel\" for=\"id_vcena\">Vyvolávací cena: <input type=\"text\" id=\"id_vcena\" name=\"vcena\" tabindex=\"5\" class=\"editfields\" value=\"";
      if (isset($p_vcena)) {echo $p_vcena;}
      echo "\" size=\"10\" maxlength=\"10\" title=\"Zadejte Vyvolávací cenu\" /><span style=\"position: relative; left: 95px; top: -2px;\">Kč</span></label>
      <div class=\"new_line_customer_up\" style=\"margin-bottom: 30px;\"></div>

      <label  title=\"Zadejte Znalecký posudek\" class=\"editlabel\" for=\"id_vposudek\">Znalecký posudek: <input type=\"file\" class=\"editfields\" name=\"vposudek\" id=\"id_vposudek\" title=\"Zadejte Znalecký posudek\" /></label>
      <div class=\"new_line_customer_up\"></div>
      <div style=\"position: relative; float: left; font-size: 12px; margin-bottom: 30px; font-style: italic; color: #000099;\">$stav_posudek</div>";
      if ($stav_posudek != "soubor nebyl nahrán") {
        echo "<div style=\"margin: 0px; border: 0px; padding: 0px; height: 0px; clear: both;\"></div>
        <input type=\"submit\" name=\"smaz_posudek\" value=\"Smaž posudek\" style=\"position: relative; float: left; width: 120px; height: 25px; margin: -25px 0px 30px 0px;\" />
        <input type=\"hidden\" name=\"posudek_cesta\" value=\"$p_odkaz_posudek\" />";
      }
      echo "<div style=\"margin: 0px; border: 0px; padding: 0px; height: 0px; clear: both;\"></div>

      <label  title=\"Zadejte Dražební vyhlášku\" class=\"editlabel\" for=\"id_vvyhlaska\">Dražební vyhláška: <input type=\"file\" class=\"editfields\" name=\"vvyhlaska\" id=\"id_vvyhlaska\" title=\"Zadejte Dražební vyhlášku\" /></label>
      <div style=\"margin: 0px; border: 0px; padding: 0px; height: 0px; clear: both;\"></div>
      <div style=\"position: relative; float: left; font-size: 12px; margin-bottom: 30px; font-style: italic; color: #000099;\">$stav_vyhlaska</div>";
      if ($stav_vyhlaska != "soubor nebyl nahrán") {
        echo "<div style=\"margin: 0px; border: 0px; padding: 0px; height: 0px; clear: both;\"></div>
        <input type=\"submit\" name=\"smaz_vyhlasku\" value=\"Smaž vyhlášku\" style=\"position: relative; float: left; width: 120px; height: 25px; margin: -25px 0px 30px 0px;\" />
        <input type=\"hidden\" name=\"vyhlaska_cesta\" value=\"$p_odkaz_vyhlaska\" />";
      }

      echo "<div class='cleaner'></div>
      <label  title=\"Zadejte Usnesení o příklepu\" class=\"editlabel\" for=\"id_vpriklep\">
      Usnesení o příklepu: <input type=\"file\" style=\"left: 150px;\" class=\"editfields\" name=\"vpriklep\" id=\"id_vpriklep\" title=\"Zadejte Usnesení o příklepu\" /></label>
      <div class='cleaner'></div>
      <div style=\"position: relative; float: left; font-size: 12px; margin-bottom: 30px; font-style: italic; color: #000099;\">{$stav_usneseni}</div>";
      if ($stav_usneseni != "soubor nebyl nahrán") {
        echo "<div class='cleaner'></div>
        <input type=\"submit\" name=\"smaz_usneseni\" value=\"Smaž usnesení\" style=\"position: relative; float: left; width: 120px; height: 25px; margin: -25px 0px 30px 0px;\" />
        <input type=\"hidden\" name=\"usneseni_cesta\" value=\"$p_odkaz_usneseni\" />";
      }

      echo "<div class='cleaner'></div>
      <label  title=\"Zadejte Oznámení o přihlášených pohledávkách\" class=\"editlabel\" for=\"id_voznameni\">
      Oznámení o přihlášených pohledávkách:
      <input type=\"file\" class=\"editfields\" style=\"left: 290px;\" name=\"voznameni\" id=\"id_voznameni\" title=\"Zadejte Oznámení o přihlášených pohledávkách\" />
      </label>
      <div class='cleaner'></div>
      <div style=\"position: relative; float: left; font-size: 12px; margin-bottom: 30px; font-style: italic; color: #000099;\">{$stav_oznameni}</div>";
      if ($stav_oznameni != "soubor nebyl nahrán") {
        echo "<div class='cleaner'></div>
              <input type=\"submit\" name=\"smaz_oznameni\" value=\"Smaž oznámení\" style=\"position: relative; float: left; width: 120px; height: 25px; margin: -25px 0px 30px 0px;\" />
              <input type=\"hidden\" name=\"oznameni_cesta\" value=\"$p_odkaz_oznameni\" />";
      }

      echo "
      <div class=\"new_line_customer_up\"></div>
      <div class='cleaner' style='height: 20px;'></div>
      <label title=\"Zadejte krátký text, pro přehled dražeb\" class=\"editlabel\" for=\"id_kratky_text\">
              <span style=\"left: 0px; position: absolute; top: -36px;\">Krátký text:</span> <textarea id=\"id_kratky_text\" name=\"kratky_text\" tabindex=\"8\" class=\"editfields\" style=\"position: relative; left: 0px; float: left; margin: 0px; padding: 0px; top: -15px; width: 500px; height: 40px; font-size: 10px; overflow: hidden; font-weight: lighter; font-family: Arial, Helvetica, sans-serif;\" cols=\"50\" rows=\"5\" title=\"Zadejte krátký text, pro přehled dražeb\">";
      if (isset($p_kratky_text)) {echo $p_kratky_text;}
      echo "</textarea></label>
      <label  title=\"Zadejte podrobný text pro přehled dražeb\" class=\"editlabel\" style=\"margin-top: 0px; clear: both; width: 140px;\" for=\"id_dlouhy_text\">Podrobný text: </label>
      <div class=\"new_line_customer_up\"></div>
      <textarea id=\"id_dlouhy_text\" name=\"dlouhy_text\" tabindex=\"9\" cols=\"50\" rows=\"5\"  title=\"Zadejte podrobný text pro přehled dražeb\" class=\"mceEditor\" style=\"position: relative; clear: both; width: 556px; min-height: 428px; margin: 0px;\">";
      if (isset($p_dlouhy_text)) {echo $p_dlouhy_text;}
      echo "</textarea>
      </div>
      <input type=\"hidden\" name=\"input_platba\" id=\"id_input_platba\" value=\"\" />
      </form>

        <div class=\"new_line_customer_up\"></div>
        <fieldset style=\"position: relative; clear: both; font-size: 14px; font-weight: 200; margin: 0px 0px 10px 20px; padding: 10px; width: 800px;\">
          <legend align=\"left\">Obrázky</legend>";
      if ($p_novy == 0) {
        echo "<form action=\"obrazky_action.php\" method=\"post\" enctype=\"multipart/form-data\">
        <input type=\"hidden\" name=\"id_drazby\" value=\"$p_id\" />
        <input type=\"hidden\" name=\"id_hlavni\" value=\"$p_obrazek\" />";
        $select_obrazky = "SELECT * FROM obrazky WHERE id_drazby=" . $p_id;
        $result_obrazky = mysql_query($select_obrazky, $link);
        $rownumber_obrazky = mysql_num_rows($result_obrazky);
        if ($rownumber_obrazky > 0) {
          $i=1;
          while ($row_sel_ordering = mysql_fetch_array($result_obrazky)) {
            echo "
            <div class=\"obrazkydiv\" style=\"background-color: ";
            if ($p_obrazek == $row_sel_ordering['id']) { echo "#CCFFFF";}
            echo ";\">
            <input title=\"Zde označte, obrázek, s kterým chcete provádět operaci\" type=\"radio\" name=\"oznaceni\" id=\"id_oznaceni_{$row_sel_ordering['id']}\" value=\"{$row_sel_ordering['id']}\" class=\"drazbyradio\" />
            <label class=\"drazbylabel\" for=\"id_oznaceni_{$row_sel_ordering['id']}\">" . $uploadsmall . "obrazek_" . $row_sel_ordering['id'] . ".jpg";
            if ($p_obrazek == $row_sel_ordering['id']) { echo "<br /><br /><span style=\"font-weight: 600;\">Tento obrázek je hlavní</span>";}
            echo "</label>
            <img src=\"" . $uploadsmall . "obrazek_" . $row_sel_ordering['id'] . ".jpg?";
            echo mt_rand (0, 9999);
            echo "\" class=\"drazby_obrazek\">
            </div>";
            if (($i % 2) == 0) {
              echo "<div class=\"new_line_customer_up\"></div>";
            }
            $i++;
          }
        }
        echo "
        <div class=\"new_line_customer_up\"></div>
        <hr />
        <input type=\"file\" class=\"drazby_file\" name=\"soubor\" id=\"id_soubor\" size=\"0\"/>";
        if ($rownumber_obrazky < 12) {
          echo "<input title=\"Přidejte další obrázek\" type=\"submit\" class=\"drazby_button\" name=\"pridat\" value=\"Přidat\" />";
        }
        echo "<input title=\"Uberte vybraný obrázek\" type=\"submit\" class=\"drazby_button\" name=\"ubrat\" value=\"Ubrat\" />
        <input title=\"Změňte vybraný obrázek\" type=\"submit\" class=\"drazby_button\" name=\"zmenit\" value=\"Změnit\" />
        <input title=\"Označte vybraný obrázek jako hlavní\" type=\"submit\" class=\"drazby_button\" name=\"hlavni\" value=\"Označit jako hlavní\" />
        <div style=\"position: relative; clear: both; padding-top: 20px; font-style: italic; font-weight: bolder; color: red;\">";
        if (isset($_GET['chyba'])) {
          if ($_GET['chyba']==1) {
            echo "zadaný obrázek je pod hranicí VGA rozlišení 640 x 480 - nebude dostatečně kvalitní";
          }
        }
        echo "</div>
        <input type=\"hidden\" name=\"input_platba\" id=\"id_input_platba\" value=\"\" />
      </form>";
      } else {
        echo "
        <div style=\"position: relative; float: left; margin: 8px 5px 0px 5px; padding: 0px; width: 100px; width: 500px;\">Obrázky bude možné přidávat až po založení nové dražby</div>";
      }
      echo "</fieldset>";
?>
