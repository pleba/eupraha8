<?php 
  include "auth_user.inc.php";
  require('../connect.php');
  require('config.php');
  require('functions_admin.php');
  $title_edit = "Editace";
if (isset($_GET['type'])){
  switch ($_GET['type']){
    case "optimalizace":
      require('php_optimalizace.php');
      $title_edit = "Editace";
    break;
    case "soukroma_exekuce":
      require('php_soukroma_exekuce.php');
      $title_edit = "Editace";
    break;
    case "pravni_uprava":
      require('php_pravni_uprava.php');
      $title_edit = "Editace";
    break;
    case "pro_dluzniky":
      require('php_pro_dluzniky.php');
      $title_edit = "Editace";
    break;
    case "pro_zamestnavatele":
      require('php_pro_zamestnavatele.php');
      $title_edit = "Editace";
    break;  
    case "exekucni_cinnost":
      require('php_exekucni_cinnost.php');
      $title_edit = "Editace";
    break;  
    case "exekutorske_zapisy":
      require('php_exekutorske_zapisy.php');
      $title_edit = "Editace";
    break;
    case "vyzvy":
      require('php_vyzvy.php');
      $title_edit = "Editace";
    break;
    case "nedorucene":
      require('php_nedorucene.php');
      $title_edit = "Editace";
    break;
     case "vyzvy_data":
      require('php_vyzvy_data.php');
      $title_edit = "Editace";
    break;
    case "nedorucene_data":
      require('php_nedorucene_data.php');
      $title_edit = "Editace";
    break;
    case "drazby_obsah":
      require('php_drazby_obsah.php');
      $title_edit = "Editace";
    break;
    case "uvodnik":
      require('php_uvodnik.php');
      $title_edit = "Editace";
    break;    
    case "kontakt":
      require('php_kontakt.php');
      $title_edit = "Editace";
    break;
    case "drazby":
      require('php_drazby.php');
      $title_edit = "Editace";
    break;
    default:
      $title_edit = "Editace";
    break;
  }
}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="Content-Language" content="cs"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="Author" content="www.artfocus.cz - Tvorba www stránek, e-shopů a SEO"/>
    <link rel="stylesheet" href="../styles/admin.css" type="text/css" media="screen, projection"/>    
    <title>Exekutorský úřad Praha 8 - administrátorské rozhraní</title>
    <script language="javascript" type="text/javascript" src="../tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
    <script language="javascript" type="text/javascript">
    /* <![CDATA[ */
    tinyMCE.init({    
    	theme : "advanced",
      mode : "textareas",
      elements : "main",
      skin : "o2k7",
      content_css : "../styles/mycontent.css",
      skin_variant : "silver",
      plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups",
      // Theme options
		theme_advanced_buttons1 : "fullscreen,|,undo,redo,|,code,removeformat,|,search,replace,|,cut,copy,paste,pastetext,pasteword,|,link,unlink,anchor,|,image,charmap,emotions,media,advhr,nonbreaking,|,forecolor,backcolor",
		theme_advanced_buttons2 : "formatselect,fontselect,fontsizeselect,|,bold,italic,underline,sub,sup,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,outdent,indent",
		theme_advanced_buttons3 : "tablecontrols",		
    	doctype : '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">',
      language : "cs",
  		theme_advanced_toolbar_location : "bottom",      
  		theme_advanced_toolbar_align : "left",
  		theme_advanced_statusbar_location : "bottom",  		
  		editor_selector : "mceEditor",
  		extended_valid_elements : "hr[class|width|size|noshade]",
			file_browser_callback : "fileBrowserCallBack",
			paste_use_dialog : false,
			theme_advanced_resizing : false,
			theme_advanced_resize_horizontal : false,
			apply_source_formatting : false,			
			force_p_newlines : true,
			entity_encoding : "raw"
    });
    
		function fileBrowserCallBack(field_name, url, type, win) {
			var connector = "http://www.eupraha8.cz/file_manager.php";

			my_field = field_name;
			my_win = win;

			switch (type) {
				case "image":
					connector += "?type=img";
					break;
				case "media":
					connector += "?type=media";
					break;
				case "flash": //for older versions of tinymce
					connector += "?type=media";
					break;
				case "file":
					connector += "?type=files";
					break;
			}

			window.open(connector, "file_manager", "modal,width=450,height=450,scrollbars=1");
		}
    /* ]]> */
    </script>
    
    <script type="text/javascript">
      var wholewhat = "";
      
      function showmenu1(menu) {
        var submenu;
        submenu = "submenu_" + menu;
        
        document.getElementById(submenu).style.display = "block";
        
        if (submenu == "submenu_2") {
          document.getElementById("submenu_1").style.display = "none";
          document.getElementById("submenu_3").style.display = "none";
        }
        if (submenu == "submenu_1") {
          document.getElementById("submenu_2").style.display = "none";          
        }  
        if (submenu == "submenu_3") {
          document.getElementById("submenu_2").style.display = "none";          
        } 
        if (submenu == "submenu_4") {
          document.getElementById("submenu_3").style.display = "none";          
        }                     
      }
      
      function hidemenu1(menu) {
        var submenu;
        submenu = "submenu_" + menu;
        document.getElementById(submenu).style.display = "none";      
      }      
     
      function hideall() {
        hidemenu1('1');
        hidemenu1('2');
        hidemenu1('3');
      }      
    </script>
  </head>
  
  <body>
    <div class="wrapper">
      <div id="menu" class="menuborder">
        <div class="statusframe">
          <h1 class="statustext" title="aktuální činnost">&nbsp;<?php echo $title_edit; ?></h1>
        </div>
        <div onmouseover="hidemenu1('1');" class="userframe">
          <span class="usertext" title="jméno uživatele"><?php  echo $_SESSION['a_name']; ?></span>
          <span class="roletext" title="role uživatele"><?php  echo $_SESSION['a_role']; ?></span>
          <span class="timetoend" title="čas do konce relace"><?php echo get_cfg_var('session.gc_maxlifetime'); ?><br />min</span>                 
        </div>        
      	<a href="#" onmouseover="showmenu1('1');" id="menu_1" class="mainmenu">
        	<span class="textmainmenu" style="display: block;">Úřední deska</span>
        </a>
 
 
  <?php
    if (($_SESSION['a_user_logged'] == "11810845CB8579759F8D4C72D444DDF332DD8529") and ($_SESSION['a_user_password'] == "28D8DD180582E064EBC824BECA831E223D4E2F18")){
  ?>
          <div id="submenu_1" class="submenuwhole" style="left: 210px; height: 100px;">        	  

            <a href="index.php?type=optimalizace" class="submenu"><span style="display: block;" class="textsubmenu">Optimalizace</span></a>
            <a href="index.php?type=vyzvy" class="submenu"><span  style="display: block;" class="textsubmenu">Výzvy - horní text</span></a>
        	  <a href="index.php?type=vyzvy_data" class="submenu"><span  style="display: block;" class="textsubmenu">Výzvy - seznam</span></a>
            <a href="index.php?type=nedorucene" class="submenu"><span  style="display: block;" class="textsubmenu">Nedoručené - horní text</span></a>
            <a href="index.php?type=nedorucene_data" class="submenu"><span  style="display: block;" class="textsubmenu">Nedoručené - seznam</span></a>
                   
        </div>
  <?php
  }else {
  ?>
 
 
        <div id="submenu_1" class="submenuwhole" style="left: 210px; height: 80px;">        	  

            <a href="index.php?type=vyzvy" class="submenu"><span  style="display: block;" class="textsubmenu">Výzvy - horní text</span></a>
        	  <a href="index.php?type=vyzvy_data" class="submenu"><span  style="display: block;" class="textsubmenu">Výzvy - seznam</span></a>
            <a href="index.php?type=nedorucene" class="submenu"><span  style="display: block;" class="textsubmenu">Nedoručené - horní text</span></a>
            <a href="index.php?type=nedorucene_data" class="submenu"><span  style="display: block;" class="textsubmenu">Nedoručené - seznam</span></a>
            
        </div>
 
 
 
 
 <?php }?>
 
      	<a id="menu_2" onmouseover="showmenu1('2');" class="mainmenu" href="#">
        	<span  style="display: block;" class="textmainmenu">Obsah</span>
        </a>    
        <div id="submenu_2" class="submenuwhole" style="left: 360px; height: 180px;">
        	  <a href="index.php?type=uvodnik" class="submenu"><span  style="display: block;" class="textsubmenu">Úvodník</span></a>
            <a href="index.php?type=soukroma_exekuce" class="submenu"><span  style="display: block;" class="textsubmenu">Soukromá exekuce</span></a>
        	  <a href="index.php?type=pravni_uprava" class="submenu"><span  style="display: block;" class="textsubmenu">Právní úprava</span></a>
        	  <a href="index.php?type=pro_dluzniky" class="submenu"><span  style="display: block;" class="textsubmenu">Pro dlužníky</span></a>
            <a href="index.php?type=pro_zamestnavatele" class="submenu"><span  style="display: block;" class="textsubmenu">Pro zaměstnavatele</span></a>
            <a href="index.php?type=exekucni_cinnost" class="submenu"><span  style="display: block;" class="textsubmenu">Exekuční činnost</span></a>
            <a href="index.php?type=exekutorske_zapisy" class="submenu"><span  style="display: block;" class="textsubmenu">Exekutorské zápisy</span></a>
        	  <a href="index.php?type=drazby_obsah" class="submenu"><span  style="display: block;" class="textsubmenu">Informace dražby</span></a>
        	  <a href="index.php?type=kontakt" class="submenu"><span  style="display: block;" class="textsubmenu">Kontakt</span></a>           
        </div>                  	
        <a id="menu_3" onmouseover="showmenu1('3');" class="mainmenu" href="index.php?type=drazby">
        	<span  style="display: block;" class="textmainmenu">Dražby</span>
        </a>
        
        <div onmouseover="hidemenu1('3');" id="menu_4" class="reservemenu">
        	<span class="textmainmenu">&nbsp;</span>
        </div>
        <a id="menu_5" class="mainmenu" href="log_in.php?go_out=1">
        	<span  style="display: block;" class="textmainmenu">Odhlásit...</span>
        </a>                            
        <div style="float: none;"></div> 
      </div>
      <div onmouseover="hideall();" class="contentplace">      
      <?php
    /*echo "GET"; print_r($_GET); echo "<br />";
    echo "POST"; print_r($_POST); echo "<br />";
    echo "SESSION"; print_r($_SESSION); echo "<br />";  */   
        if (isset($_GET['type'])){
          switch ($_GET['type']){
            case "optimalizace":
              require('html_optimalizace.php');
            break;
            case "soukroma_exekuce":
              require('html_soukroma_exekuce.php');
            break;
            case "pravni_uprava":
              require('html_pravni_uprava.php');
            break;
            case "pro_dluzniky":
              require('html_pro_dluzniky.php');
            break;
            case "pro_zamestnavatele":
              require('html_pro_zamestnavatele.php');
            break;
            case "exekucni_cinnost":
              require('html_exekucni_cinnost.php');
            break;
            case "exekutorske_zapisy":
              require('html_exekutorske_zapisy.php');
            break;
            case "vyzvy":
              require('html_vyzvy.php');
            break;
            case "nedorucene":
              require('html_nedorucene.php');
            break;
            case "vyzvy_data":
              require('html_vyzvy_data.php');
            break;
            case "nedorucene_data":
              require('html_nedorucene_data.php');
            break;
            case "drazby_obsah":
              require('html_drazby_obsah.php');
            break;                        
            case "kontakt":
              require('html_kontakt.php');
            break;
            case "uvodnik":
              require('html_uvodnik.php');
            break;            
            case "drazby":
              if ($editing == 0) {require ('list_drazby.php');}
              else {require ('edit_drazby.php');}          
            break;
            default:
            break;
          }
        }   
      ?>
      </div>
    </div>    
  </body>
</html>
