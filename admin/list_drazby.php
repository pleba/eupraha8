<?php
include "auth_user.inc.php";

echo "

    <script type=\"text/javascript\">
      
      function submitform() {
         //window.alert(\"jsem tu\");
         document.getElementById('id_input_filtr').value = 'filtruj';
         if (checkform(document.mainform)) {
           document.mainform.submit();
         }
      }
            
      function checkform(f) {
        var errortext = \"\";
        var datum_f = /^[0-9]{2}\.[0-9]{2}\.[0-9]{4}\$/;
        
        if (f.elements[\"odkdy\"].value != \"\") {
          if (!datum_f.test(f.elements[\"odkdy\"].value)) errortext += \"datum odkdy musí být uvedeno ve tvaru 26.01.2008\" + \"\\n\";
        }
        if (f.elements[\"dokdy\"].value != \"\") {
          if (!datum_f.test(f.elements[\"dokdy\"].value)) errortext += \"datum dokdy musí být uvedeno ve tvaru 26.01.2008\" + \"\\n\";
        }  
                    
        if(errortext!=\"\"){          
          window.alert(errortext);
          return false;     
        } else {
          return true;
        }   
      }     
    </script>   
  
 		<div style=\"position: relative; float: left; margin-top: 10px; margin-left: 20px;\">Obsah webstránky - Seznam objednávek: </div>
 		<div style=\"clear: both;\"> </div>
    <div style=\"position: relative; float: left; width: 840px; margin: 25px 0px 0px 20px; padding: 8px; background-color: #CCFFCC; border: 2px outset;\"><span style=\"position: relative; top: 0px; left: 5px; font-weight: 600;\">FILTR: </span>
  <form action=\"index.php?type=drazby\" method=\"post\" name=\"mainform\" style=\"padding: 0px; border: 0px; margin: 0px;\">  
  <input style=\"margin: 0px; position: absolute; left: 150px; top: 4px;\" type=\"image\" src=\"../img/nova_drazba.gif\"  name=\"novy\" value=\"Nový\" title=\"Založ novou dražbu\" />
  <input style=\"margin: 0px; position: absolute; left: 290px; top: 4px;\" type=\"image\" src=\"../img/resetuj.gif\" name=\"reset\" value=\"R\" title=\"Vyresetuje formulář\" />  
  <input style=\"margin: 0px; position: absolute; left: 427px; top: 4px;\" type=\"image\" src=\"../img/ulozit.gif\"  name=\"uloz\" value=\"Ulož\" title=\"Ulož filtr i třídění pro příště\" />
  <hr />
  <p style=\"font-size: 10px; margin: 4px 0px 0px 0px;\">
  &nbsp;NADPIS:&nbsp;
  <input onchange=\"submitform();\" title=\"Zadejte počáteční písmena nadpisu\" type=\"text\" name=\"nadpis\" id=\"id_nadpis\" size=\"13\" maxlength=\"20\" value=\"";  
  if (isset($_SESSION['s_nadpis'])) {
    echo $_SESSION['s_nadpis'];
  }  
  echo "\" /> 
  
  &nbsp;&nbsp;&nbsp;OD KDY:&nbsp;
  <input onchange=\"submitform();\" title=\"Zadejte datum od kdy včetně ve formátu 26.01.2008\" type=\"text\" name=\"odkdy\" id=\"id_odkdy\" size=\"10\" maxlength=\"10\" value=\"";
  if (isset($_SESSION['s_odkdy'])) {
    echo $_SESSION['s_odkdy'];
  } else { echo "01.01.2008";}
  echo "\" /> 
  
  &nbsp;&nbsp;&nbsp;DO KDY:&nbsp;
  <input onchange=\"submitform();\" title=\"Zadejte datum do kdy včetně ve formátu 26.01.2008\" type=\"text\" name=\"dokdy\" id=\"id_dokdy\" size=\"10\" maxlength=\"10\" value=\"";
  if (isset($_SESSION['s_dokdy'])) {
    echo $_SESSION['s_dokdy'];
  }
  echo "\" />

  </p>
  <p style=\"font-size: 10px; margin: 4px 0px 0px 0px;\">
  
  &nbsp;ODHAD. CENA:&nbsp;
  <select onchange=\"submitform();\" style=\"font-size: 12px;\" size=\"1\" name=\"ocena\" id=\"id_ocena\">
    <option value=\"\" "; 
      if (isset($_SESSION['s_ocena'])) {
        if ($_SESSION['s_ocena'] == "") {
          echo " selected=\"selected\"";
        }
      }
    echo ">všechny</option>
    <option value=\"49999\" "; 
      if (isset($_SESSION['s_ocena'])) {
        if ($_SESSION['s_ocena'] == 49999) {
          echo " selected=\"selected\"";
        }
      }
    echo ">0 - 49 999 Kč</option>
    <option value=\"99999\" "; 
      if (isset($_SESSION['s_ocena'])) {
        if ($_SESSION['s_ocena'] == 99999) {
          echo " selected=\"selected\"";
        }
      }
    echo ">50 000 - 99 999 Kč</option>
    <option value=\"499999\" "; 
      if (isset($_SESSION['s_ocena'])) {
        if ($_SESSION['s_ocena'] == 499999) {
          echo " selected=\"selected\"";
        }
      }
    echo ">100 000 - 499 999 Kč</option>
    <option value=\"999999\" "; 
      if (isset($_SESSION['s_ocena'])) {
        if ($_SESSION['s_ocena'] == 999999) {
          echo " selected=\"selected\"";
        }
      }
    echo ">500 000 - 999 999 Kč</option>
    <option value=\"1000000\" "; 
      if (isset($_SESSION['s_ocena'])) {
        if ($_SESSION['s_ocena'] == 1000000) {
          echo " selected=\"selected\"";
        }
      }
    echo ">1 000 000 Kč a více</option>
  </select>
  
  &nbsp;&nbsp;&nbsp;VYVOL. CENA:&nbsp;
  <select onchange=\"submitform();\" style=\"font-size: 12px;\" size=\"1\" name=\"vcena\" id=\"id_vcena\">
    <option value=\"\" "; 
      if (isset($_SESSION['s_vcena'])) {
        if ($_SESSION['s_vcena'] == "") {
          echo " selected=\"selected\"";
        }
      }
    echo ">všechny</option>
    <option value=\"49999\" "; 
      if (isset($_SESSION['s_vcena'])) {
        if ($_SESSION['s_vcena'] == 49999) {
          echo " selected=\"selected\"";
        }
      }
    echo ">0 - 49 999 Kč</option>
    <option value=\"99999\" "; 
      if (isset($_SESSION['s_vcena'])) {
        if ($_SESSION['s_vcena'] == 99999) {
          echo " selected=\"selected\"";
        }
      }
    echo ">50 000 - 99 999 Kč</option>
    <option value=\"499999\" "; 
      if (isset($_SESSION['s_vcena'])) {
        if ($_SESSION['s_vcena'] == 499999) {
          echo " selected=\"selected\"";
        }
      }
    echo ">100 000 - 499 999 Kč</option>
    <option value=\"999999\" "; 
      if (isset($_SESSION['s_vcena'])) {
        if ($_SESSION['s_vcena'] == 999999) {
          echo " selected=\"selected\"";
        }
      }
    echo ">500 000 - 999 999 Kč</option>
    <option value=\"1000000\" "; 
      if (isset($_SESSION['s_vcena'])) {
        if ($_SESSION['s_vcena'] == 1000000) {
          echo " selected=\"selected\"";
        }
      }
    echo ">1 000 000 Kč a více</option>
  </select>

  &nbsp;&nbsp;&nbsp;STAV:&nbsp;
  <select onchange=\"submitform();\" style=\"font-size: 12px;\" size=\"1\" name=\"is_valid\" id=\"id_is_valid\">
    <option value=\"\" "; 
      if (isset($_SESSION['s_is_valid'])) {
        if ($_SESSION['s_is_valid'] == "") {
          echo " selected=\"selected\"";
        }
      }
    echo ">všechny</option>
    <option value=\"aktivní\" "; 
      if (isset($_SESSION['s_is_valid'])) {
        if ($_SESSION['s_is_valid'] == "aktivní") {
          echo " selected=\"selected\"";
        } 
      } else {
        echo " selected=\"selected\"";
      }
    echo ">aktivní</option>
    <option value=\"zrušené\" "; 
      if (isset($_SESSION['s_is_valid'])) {
        if ($_SESSION['s_is_valid'] == "zrušené") {
          echo " selected=\"selected\"";
        }
      }
    echo ">zrušené</option>
  </select>
  
  &nbsp;&nbsp;&nbsp;VYMAZÁNO:&nbsp;
  <select onchange=\"submitform();\" style=\"font-size: 12px;\" size=\"1\" name=\"is_deleted\" id=\"id_is_deleted\">
    <option value=\"\" "; 
      if (isset($_SESSION['s_is_deleted'])) {
        if ($_SESSION['s_is_deleted'] == "") {
          echo " selected=\"selected\"";
        }
      }
    echo ">všechny</option>
    <option value=\"aktivní\" "; 
      if (isset($_SESSION['s_is_deleted'])) {
        if ($_SESSION['s_is_deleted'] == "aktivní") {
          echo " selected=\"selected\"";
        } 
      } else {
        echo " selected=\"selected\"";
      }
    echo ">aktivní</option>
    <option value=\"smazané\" "; 
      if (isset($_SESSION['s_is_deleted'])) {
        if ($_SESSION['s_is_deleted'] == "smazané") {
          echo " selected=\"selected\"";
        }
      }
    echo ">smazané</option>
  </select>
  </p>  
  <hr />
  <span style=\"position: relative; top: 0px; left: 5px; font-weight: 600;\">TŘÍDĚNÍ: </span>
  <span style=\"position: relative; top: 0px; left: 5px; font-weight: 200; font-size: 12px;\">podle ";
  if (isset($_SESSION['s_order'])) {
   switch ($_SESSION['s_order']) {
     case "nadpis":
       echo "nadpisu ";
       break;
     case "datum_a_cas";  
       echo "data a času ";
       break;
     case "odhadovana_cena";  
       echo "odhadované ceny ";
       break;
     case "vyvolavaci_cena";  
       echo "vyvolávací ceny ";
       break;
     default:
       echo "data a času ";
       break;  
   }
  } 
  if (isset($_SESSION['s_how'])) {  
   switch ($_SESSION['s_how']) {
     case "down":
       echo "sestupně";
       break;
     case "up";  
       echo "vzestupně";
       break;
     default:
       echo "vzestupně";
       break;  
   }
  }  
  echo "</span>  
  <input type=\"hidden\" name=\"input_filtr\" id=\"id_input_filtr\" value=\"\" />
  </form>";
  
//filtrování nadpisu
if (isset($_SESSION['s_nadpis'])) {
  if ($_SESSION['s_nadpis'] != "") {
    $filtr_nadpis = " AND nadpis LIKE '" . $_SESSION['s_nadpis'] . "%'";
  } else {
    $filtr_nadpis = "";
  }
} else {
  $filtr_nadpis = "";
}

//filtrování od kdy
if (isset($_SESSION['s_odkdy'])) {
  if ($_SESSION['s_odkdy'] != "") {
    $od_kdy_sestav = substr($_SESSION['s_odkdy'],6 , 4) . "-" . substr($_SESSION['s_odkdy'],3 , 2) . "-" . substr($_SESSION['s_odkdy'],0 , 2) . " 00:00";
    $filtr_odkdy = " AND datum_a_cas > '" . $od_kdy_sestav . "'";
  } else {
    $filtr_odkdy = "";
  }
} else {
  $filtr_odkdy = "";
}

//filtrování do kdy
if (isset($_SESSION['s_dokdy'])) {
  if ($_SESSION['s_dokdy'] != "") {
    $do_kdy_sestav = substr($_SESSION['s_dokdy'],6 , 4) . "-" . substr($_SESSION['s_dokdy'],3 , 2) . "-" . substr($_SESSION['s_dokdy'],0 , 2) . " 23:59";
    $filtr_dokdy = " AND datum_a_cas < '" . $do_kdy_sestav . "'";
  } else {
    $filtr_dokdy = "";
  }
} else {
  $filtr_dokdy = "";
}

//filtrování podle odhadované ceny
if (isset($_SESSION['s_ocena'])) {
  if ($_SESSION['s_ocena'] != "") {
    switch ($_SESSION['s_ocena']) {
      case "49999":
        $filtr_ocena = " AND odhadovana_cena < 50000";
      break;
      case "99999":
        $filtr_ocena = " AND ((odhadovana_cena >= 50000) AND (odhadovana_cena < 100000))";
      break;
      case "499999":
        $filtr_ocena = " AND ((odhadovana_cena >= 100000) AND (odhadovana_cena < 500000))";
      break;
      case "999999":
        $filtr_ocena = " AND ((odhadovana_cena >= 500000) AND (odhadovana_cena < 1000000))";
      break;
      case "1000000":
        $filtr_ocena = " AND odhadovana_cena >= 1000000";
      break;
      default:
        $filtr_ocena = "";
      break;
    }    
  } else {
    $filtr_ocena = "";
  }
} else {
  $filtr_ocena = "";
}

//filtrování podle vyvolávací ceny
if (isset($_SESSION['s_vcena'])) {
  if ($_SESSION['s_vcena'] != "") {
    switch ($_SESSION['s_vcena']) {
      case "49999":
        $filtr_vcena = " AND vyvolavaci_cena < 50000";
      break;
      case "99999":
        $filtr_vcena = " AND ((vyvolavaci_cena >= 50000) AND (vyvolavaci_cena < 100000))";
      break;
      case "499999":
        $filtr_vcena = " AND ((vyvolavaci_cena >= 100000) AND (vyvolavaci_cena < 500000))";
      break;
      case "999999":
        $filtr_vcena = " AND ((vyvolavaci_cena >= 500000) AND (vyvolavaci_cena < 1000000))";
      break;
      case "1000000":
        $filtr_vcena = " AND vyvolavaci_cena >= 1000000";
      break;
      default:
        $filtr_vcena = "";
      break;
    }    
  } else {
    $filtr_vcena = "";
  }
} else {
  $filtr_vcena = "";
}

//filtrování podle platnosti dražby
if (isset($_SESSION['s_is_valid'])) {
  switch ($_SESSION['s_is_valid']) {
    case "aktivní":
      $filtr_validity = " AND stav = 0";
      break;
    case "zrušené":
      $filtr_validity = " AND stav = 1";
      break;  
    case "":
      $filtr_validity = "";
      break;
    default:
      $filtr_validity = " AND stav = 0";
      break;  
  } 
} else {
  $filtr_validity = " AND stav = 0";
}

//filtrování podle smazanosti dražby
if (isset($_SESSION['s_is_deleted'])) {
  switch ($_SESSION['s_is_deleted']) {
    case "aktivní":
      $filtr_deleted = " AND cancel = 0";
      break;
    case "smazané":
      $filtr_deleted = " AND cancel = 1";
      break;  
    case "":
      $filtr_deleted = "";
      break;
    default:
      $filtr_deleted = " AND cancel = 0";
      break;  
  } 
} else {
  $filtr_deleted = " AND cancel = 0";
}

//řazení
if ((isset($_SESSION['s_order'])) and (isset($_SESSION['s_how']))) {
 switch ($_SESSION['s_order']) {
   case "nadpis":
     $order_drazby = "nadpis ";
     break;
   case "datum_a_cas";  
     $order_drazby = "datum_a_cas ";
     break;
   case "odhadovana_cena";  
     $order_drazby = "odhadovana_cena ";
     break;
   case "vyvolavaci_cena";  
     $order_drazby = "vyvolavaci_cena ";
     break;          
   default:
     $order_drazby = "datum_a_cas ";
     break;  
 }

 switch ($_SESSION['s_how']) {
   case "down":
     $order_drazby .= "DESC";
     break;
   case "up";  
     $order_drazby .= "ASC";
     break;
   default:
     $order_drazby .= "ASC";
     break;  
 }
} else {
 $order_drazby="datum_a_cas ASC";
}

$sql_l = "SELECT id, nadpis, datum_a_cas, odhadovana_cena, vyvolavaci_cena, stav, cancel  FROM drazby WHERE id>0" . $filtr_nadpis . $filtr_odkdy . $filtr_dokdy . $filtr_ocena . $filtr_vcena . $filtr_validity . $filtr_deleted . " ORDER BY " . $order_drazby;


if (isset($sql_l)) {
  echo "<span style=\"position: relative; clear: both; font-size: 10px; color: #A5A5A5;\">" . $sql_l . "</span><br /><br />"; 
}
/*
if (isset($sql_filtering)) {
  echo "<span style=\"position: relative; float: left; font-size: 10px; color: #A5A5A5;\">" . $sql_filtering . "</span><br /><br />"; 
}
*/
$result_l = mysql_query($sql_l, $link)
  or die(mysql_error($link));
    
$filter_amount = mysql_num_rows($result_l);
echo "<div style=\"position: absolute; top: 8px; right: 15px; width: 240px; height: 20px; border: 0px; background-color: #CEFFCE; font-size: 12px;\">Počet zobrazených dražeb: <span style=\"font-size: 16px; font-weight: 600;\">" . $filter_amount . "</span></div>";  
  
  echo "  
  </div>
  <div style=\"position: relative; clear: both; margin: 10px 20px 0px 20px;\">";  
  
  if (isset($p_image_upload_error)) {
    if ($p_image_upload_error != 0) {
      echo "<div style=\"background-color: #FFFFFF; position: relative; float: left; margin: 10px 0px 0px 20px; height: 16px; width: 300px; border: inset; text-align: center; padding: 4px 0px; font-size: 12px; color: red;\">";                         
      switch ($p_image_upload_error) {
        case 2: echo "Vložený soubor nemá příponu doc, nebo pdf - proto nebyl vložen!";
        break;
        case 3: echo "Vložený soubor může mýt nebezpečný obsah - proto nebyl vložen!";
        break;
      }          
      echo "</div>
      <div style=\"position: relative; clear: both; margin: 10px 20px 0px 20px;\">";  
    }
  }
  
  echo "
    <table  style=\"margin: 10px 10px; font-size: 12px;\" width=\"710\" cellpadding=\"0\" cellspacing=\"0\">
    <tr style=\"background-color: #dedfde; height: 24px;\">
      <td style=\"border: black solid 1px; width: 60px; text-align: center;\">p.č.</td>
      <td style=\"border: black solid 1px; width: 140px; text-align: left;\">&nbsp;&nbsp;<a href=\"index.php?type=drazby&amp;order=nadpis&amp;how=down\" title=\"seřadit sestupně\"><img src=\"../img/a_down.png\" title=\"seřadit sestupně\" style=\"border: 0px;\" alt=\"\" /></a>&nbsp;<a href=\"index.php?type=drazby&amp;order=nadpis&amp;how=up\" title=\"seřadit sestupně\"><img src=\"../img/a_up.png\" title=\"seřadit vzestupně\" style=\"border: 0px;\" alt=\"\" /></a>&nbsp;Nadpis</td>
      <td style=\"border: black solid 1px; width: 150px; text-align: center;\"><a href=\"index.php?type=drazby&amp;order=datum_a_cas&amp;how=down\" title=\"seřadit sestupně\"><img src=\"../img/a_down.png\" title=\"seřadit sestupně\" style=\"border: 0px;\" alt=\"\" /></a>&nbsp;<a href=\"index.php?type=drazby&amp;order=datum_a_cas&amp;how=up\" title=\"seřadit sestupně\"><img src=\"../img/a_up.png\" title=\"seřadit vzestupně\" style=\"border: 0px;\" alt=\"\" /></a>&nbsp;Datum a čas</td>
      <td style=\"border: black solid 1px; width: 150px; text-align: center;\" align=\"left\">&nbsp;&nbsp;<a href=\"index.php?type=drazby&amp;order=odhadovana_cena&amp;how=down\" title=\"seřadit sestupně\"><img src=\"../img/a_down.png\" title=\"seřadit sestupně\" style=\"border: 0px;\" alt=\"\" /></a>&nbsp;<a href=\"index.php?type=drazby&amp;order=odhadovana_cena&amp;how=up\" title=\"seřadit sestupně\"><img src=\"../img/a_up.png\" title=\"seřadit vzestupně\" style=\"border: 0px;\" alt=\"\" /></a>&nbsp;Odhadovaná cena</td>
      <td style=\"border: black solid 1px; width: 180px; text-align: center;\" align=\"right\"><a href=\"index.php?type=drazby&amp;order=vyvolavaci_cena&amp;how=down\" title=\"seřadit sestupně\"><img src=\"../img/a_down.png\" title=\"seřadit sestupně\" style=\"border: 0px;\" alt=\"\" /></a>&nbsp;<a href=\"index.php?type=drazby&amp;order=vyvolavaci_cena&amp;how=up\" title=\"seřadit sestupně\"><img src=\"../img/a_up.png\" title=\"seřadit vzestupně\" style=\"border: 0px;\" alt=\"\" /></a>&nbsp;Vyvolávací cena</td>  
      </tr>";
    
    $i=1;
    while ($row_l = mysql_fetch_array($result_l)) {
      $p_id=$row_l['id'];
      $p_nadpis=$row_l['nadpis'];
      $p_datum_a_cas=substr($row_l['datum_a_cas'],8 , 2) . "." . substr($row_l['datum_a_cas'],5 , 2) . "." . substr($row_l['datum_a_cas'],0 , 4) . substr($row_l['datum_a_cas'],10 , 6);
      $p_odhadovana_cena=$row_l['odhadovana_cena'];
      $p_vyvolavaci_cena=$row_l['vyvolavaci_cena'];
      $p_stav=$row_l['stav']; 
      $p_cancel=$row_l['cancel']; 
    
      echo "
      <tr class=\"radkatb\">
      <td title=\"Pořadové číslo v tomto zobrazení\" style=\"border: black solid 1px; width: 75px; padding: 0px 3px 0px 0px;\"><span style=\" position: relative; float: left; margin. 0px; padding: 0px; border: 0px;\">&nbsp;$i.</span><a href=\"index.php?type=drazby&amp;id_drazby=$p_id\" title=\"NÁHLED\" style=\"position: relative; float: right; background-color: transparent; border: 0px; margin: 0px; padding: 0px;\"><img style=\"border: 0px; margin: 0px; padding: 0px;\" src=\"../img/nahled.gif\" alt=\"\" title=\"\" /></a></td>
      <td title=\"Nadpis dražby\" style=\"border: black solid 1px; width: 140px;\" valign=\"middle\" align=\"left\">&nbsp;&nbsp;<a href=\"index.php?type=drazby&amp;id_drazby=$p_id\" title=\"NÁHLED\" style=\"background-color: transparent;\">$p_nadpis</a></td>
      <td title=\"Datum a čas dražby\" style=\"border: black solid 1px; width: 150px;\" valign=\"middle\" align=\"center\"><a href=\"index.php?type=drazby&amp;id_drazby=$p_id\" title=\"NÁHLED\" style=\"background-color: transparent;\">$p_datum_a_cas</a></td>
      <td title=\"Odhadovaná cena\" style=\"border: black solid 1px; width: 150px; font-size: 12px;\" valign=\"middle\" align=\"right\"><a href=\"index.php?type=drazby&amp;id_drazby=$p_id\" title=\"NÁHLED\" style=\"background-color: transparent;\">$p_odhadovana_cena&nbsp;Kč</a>&nbsp;</td>
      <td title=\"Vyvolávací cena\" style=\"border: black solid 1px; width: 180px;\" valign=\"middle\" align=\"right\"><a href=\"index.php?type=drazby&amp;id_drazby=$p_id\" title=\"NÁHLED\" style=\"background-color: transparent;\">$p_vyvolavaci_cena&nbsp;Kč</a></td>
      </tr>";
      $i++;
    }    
    echo "
    </table>    
  </div>";
  
?>
