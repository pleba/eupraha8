<?php
include "auth_user.inc.php";
require('../connect.php');
require('config.php');
require('functions_admin.php');

if (isset($_POST['ubrat'])) {
  if ($_POST['ubrat'] == "Ubrat") {
    if ((isset($_POST['id_drazby'])) and (isset($_POST['oznaceni'])) and (isset($_POST['id_hlavni']))) {
      $id_drazby = $_POST['id_drazby'];
      $id_obrazku = $_POST['oznaceni'];
      $id_hlavni = $_POST['id_hlavni'];
      
      $sqld = "DELETE FROM obrazky WHERE id = '$id_obrazku' AND id_drazby = '$id_drazby' LIMIT 1";
      $result_ddelete = mysql_query($sqld, $link)
        or die("Invalid query: " . mysql_error($link));      
  
      $originalfilename = $uploadoriginal . "obrazek_" . $id_obrazku . ".jpg";
      if (file_exists($originalfilename)){ unlink($originalfilename);}
      
      $bigfilename = $uploadbig . "obrazek_" . $id_obrazku . ".jpg";
      if (file_exists($bigfilename)){ unlink($bigfilename);}
      
      $middlefilename = $uploadmiddle . "obrazek_" . $id_obrazku . ".jpg";
      if (file_exists($middlefilename)){ unlink($middlefilename);}
      
      $smallfilename = $uploadsmall . "obrazek_" . $id_obrazku . ".jpg";
      if (file_exists($smallfilename)){ unlink($smallfilename);}  
      
      if ($id_hlavni == $id_obrazku) {
        $sql_update="UPDATE drazby SET id_obrazek='0' WHERE id=" . $id_drazby;
        $result_update = mysql_query($sql_update, $link)
          or die(mysql_error($link));        
      }
    }    
  }
}

if (isset($_POST['hlavni'])) {
  if ($_POST['hlavni'] == "Označit jako hlavní") {
    if ((isset($_POST['id_drazby'])) and (isset($_POST['oznaceni']))) {
      $id_drazby = $_POST['id_drazby'];
      $id_obrazku = $_POST['oznaceni'];
      $sql_update="UPDATE drazby SET id_obrazek='$id_obrazku' WHERE id=" . $id_drazby;
      $result_update = mysql_query($sql_update, $link)
        or die(mysql_error($link));        
    } 
  }
}  

if (isset($_POST['zmenit'])) {
  if ($_POST['zmenit'] == "Změnit") {
    if ((isset($_POST['id_drazby'])) and (isset($_POST['oznaceni']))) {
      $id_drazby = $_POST['id_drazby'];
      $id_obrazku = $_POST['oznaceni'];
      
      if ($_FILES['soubor']['name'] != ""){
        $p_image_upload_error = 0;
        $imageinfo = getimagesize($_FILES['soubor']['tmp_name']);
        if(($imageinfo['mime'] != 'image/gif') && ($imageinfo['mime'] != 'image/jpg') && ($imageinfo['mime'] != 'image/jpeg') && ($imageinfo['mime'] != 'image/png')) {
          $p_image_upload_error = 2;          
        }        
        $blacklist = array(".php", ".phtml", ".php3", ".php4");
        foreach ($blacklist as $item) {
          if(preg_match("/$item\$/i", $_FILES['soubor']['name'])) {
            $p_image_upload_error = 3;            
          }
        }
        $uploadfile = $uploadoriginal . basename($_FILES['soubor']['name']);
        if (move_uploaded_file($_FILES['soubor']['tmp_name'], $uploadfile)) {   
          list($width, $height, $type, $attr) = getimagesize($uploadfile);   
        } else { $p_image_upload_error = 4;}
        if ($type > 3) { $p_image_upload_error = 1;
        } else {
          if (($width < 640) and ($height < 480)) {
          $chyba = 1;;
        } else { $chyba = 0;}
          $newfilename = $uploadoriginal . "obrazek_" . $id_obrazku . ".jpg";
          if (file_exists($newfilename)){ unlink($newfilename); }          
          if ($type == 2) {
            $image_old = imagecreatefromjpeg($uploadfile);
          } elseif ($type == 1) {
            $image_old = imagecreatefromgif($uploadfile);
          } elseif ($type == 3) {
            $image_old = imagecreatefrompng($uploadfile);
          }
          $size_ratio = $width / $height;
          if ($size_ratio > 1) {
            if ($width > 900) {
              $new_width = 900;
              $new_height = round(900/$size_ratio);
            } else {
              $new_width = $width;
              $new_height = round($width/$size_ratio);
            }  
          } elseif ($size_ratio < 1) {
            if ($height > 700) {
              $new_width = round(700*$size_ratio);
              $new_height = 700;            
            } else {
              $new_width = round($height*$size_ratio);
              $new_height = $height;
            }
          } else {
            if ($height > 700) {
              $new_width = 700;
              $new_height = 700;
            } else {
              $new_width = $width;
              $new_height = $height;
            }
          }
          $image_jpg = imagecreatetruecolor($new_width, $new_height);
          imagecopyresampled($image_jpg, $image_old, 0, 0, 0, 0,
          $new_width, $new_height, $width, $height);
          imagejpeg($image_jpg, $newfilename);
          imagedestroy($image_old);
          imagedestroy($image_jpg);
          unlink($uploadfile);

          make_thumbnail($newfilename, $new_width, $new_height, 280, 210, $uploadbig, $id_obrazku);         
          make_thumbnail($newfilename, $new_width, $new_height, 95, 70, $uploadmiddle, $id_obrazku);       
          make_thumbnail($newfilename, $new_width, $new_height, 59, 44, $uploadsmall, $id_obrazku);
          
        }        
      }
    }
  }
}  

if (isset($_POST['pridat'])) {
  if ($_POST['pridat'] == "Přidat") {
    $id_drazby = $_POST['id_drazby'];
    
    if ($_FILES['soubor']['name'] != ""){
        
      $p_image_upload_error = 0;

      $imageinfo = getimagesize($_FILES['soubor']['tmp_name']);
      if(($imageinfo['mime'] != 'image/gif') && ($imageinfo['mime'] != 'image/jpg') && ($imageinfo['mime'] != 'image/jpeg') && ($imageinfo['mime'] != 'image/png')) {
        $p_image_upload_error = 2;          
      }        
      $blacklist = array(".php", ".phtml", ".php3", ".php4");
      foreach ($blacklist as $item) {
        if(preg_match("/$item\$/i", $_FILES['soubor']['name'])) {
          $p_image_upload_error = 3;            
        }
      }
      $uploadfile = $uploadoriginal . basename($_FILES['soubor']['name']);
      if (move_uploaded_file($_FILES['soubor']['tmp_name'], $uploadfile)) {   
        list($width, $height, $type, $attr) = getimagesize($uploadfile);   
      } else { $p_image_upload_error = 4; }
      if ($type > 3) { $p_image_upload_error = 1;
      } else {
        if (($width < 640) and ($height < 480)) {
          $chyba = 1;
        } else { $chyba = 0;}
        $sql_insert_obrazek = "INSERT INTO obrazky SET id_drazby='$id_drazby'";
        $insertresults = mysql_query($sql_insert_obrazek, $link)
          or die(mysql_error($link)); 
        $last_id = mysql_insert_id ($link);        
        $newfilename = $uploadoriginal . "obrazek_" . $last_id . ".jpg";
        if (file_exists($newfilename)){ unlink($newfilename);}          
        if ($type == 2) {
          $image_old = imagecreatefromjpeg($uploadfile);
        } elseif ($type == 1) {
          $image_old = imagecreatefromgif($uploadfile);
        } elseif ($type == 3) {
          $image_old = imagecreatefrompng($uploadfile);
        }
        $size_ratio = $width / $height;
        if ($size_ratio > 1) {
          if ($width > 900) {
            $new_width = 900;
            $new_height = round(900/$size_ratio);
          } else {
            $new_width = $width;
            $new_height = round($width/$size_ratio);
          }  
        } elseif ($size_ratio < 1) {
          if ($height > 700) {
            $new_width = round(700*$size_ratio);
            $new_height = 700;            
          } else {
            $new_width = round($height*$size_ratio);
            $new_height = $height;
          }
        } else {
          if ($height > 700) {
            $new_width = 700;
            $new_height = 700;
          } else {
            $new_width = $width;
            $new_height = $height;
          }
        }
        $image_jpg = imagecreatetruecolor($new_width, $new_height);
        imagecopyresampled($image_jpg, $image_old, 0, 0, 0, 0,
        $new_width, $new_height, $width, $height);
        imagejpeg($image_jpg, $newfilename);
        imagedestroy($image_old);
        imagedestroy($image_jpg);
        unlink($uploadfile);

        make_thumbnail($newfilename, $new_width, $new_height, 280, 210, $uploadbig, $last_id);         
        make_thumbnail($newfilename, $new_width, $new_height, 95, 70, $uploadmiddle, $last_id);       
        make_thumbnail($newfilename, $new_width, $new_height, 59, 44, $uploadsmall, $last_id);        
      }       
    }
  }
}
  //echo $p_image_upload_error;

  if (isset($id_drazby)) {
    $redirect = 'index.php?type=drazby&id_drazby=' . $id_drazby . '&chyba=' . $chyba;
  } else {
    $redirect = 'index.php?type=drazby';
  }  
  header("Location: $redirect"); 
?>
