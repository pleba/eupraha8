<?php
if (session_id() == ""){
  session_start();
}

$utf_header= "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"cs\" lang=\"cs\">
  <head>
    <meta http-equiv=\"Content-Language\" content=\"cs\"/>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>
    <meta name=\"Author\" content=\"www.artfocus.cz - Tvorba www stránek, e-shopů a SEO\"/>
    <link rel=\"stylesheet\" href=\"../css/admin.css\" type=\"text/css\" media=\"screen, projection\"/>    
    <title>Exekutorský úřad Praha 8 - Administrace</title>
  </head>
  <body style=\" margin: 20px; text-align: center;\"> ";
$utf_footer="</body>
</html>";

if ((isset($_SESSION['a_user_logged']) &&
        $_SESSION['a_user_logged'] != "") ||
        (isset($_SESSION['a_user_password']) &&
         $_SESSION['a_user_password'] != "")) {
        } else {
          $redirect = $_SERVER['PHP_SELF'];
          header("Refresh: 2; URL=log_in.php?redirect=$redirect");
          echo $utf_header;
          echo "Nejste přihlášen, přesměrováváme vás pro přihlášení<br />";
          echo "(Pokud váš browser nepodporuje přesměrovávání <a href=\"log_in.php?redirect=$redirect\">klikněte sem</a>)";
          echo $utf_footer;
          die();
        }
?>
