<?php
  
    function make_thumbnail($original_image, $original_width, $original_height, $new_width, $new_height, $dir_tosave, $id_indbase) {
    
    $ratio_original = $original_width / $original_height;
    $ratio_new = $new_width / $new_height;
    
    if ($ratio_original > $ratio_new) {
      $newx=$new_width;
      $newy=$new_width / $ratio_original;
      $destinationx=0;
      $destinationy=($new_height-$newy)/2;
    } elseif ($ratio_original == $ratio_new) {
      $newx=$new_width;
      $newy=$new_height;
      $destinationx=0;
      $destinationy=0;
    } else {
      $newx=$new_height * $ratio_original;
      $newy=$new_height;
      $destinationx=($new_width-$newx)/2;
      $destinationy=0;
    }
    
    $newthumbname = $dir_tosave . "obrazek_" . $id_indbase . ".jpg";  
    $largeimage = imagecreatefromjpeg($original_image);
    $thumb = imagecreatetruecolor($new_width, $new_height);
    
    $white = imagecolorallocate($thumb, 255, 255, 255);
    imagefill($thumb, 0, 0, $white);  
  
    imagecopyresampled($thumb, $largeimage, $destinationx, $destinationy, 0, 0,
    $newx, $newy, $original_width, $original_height);
    if (file_exists($newthumbname)){
      unlink($newthumbname);
    }          
    $return = imagejpeg($thumb, $newthumbname);
    imagedestroy($largeimage);
    imagedestroy($thumb);  
    if ($return) {
      return "sp?" . $newthumbname;
    } else {
      return "nesp?" . $newthumbname;
    }
    
  }
  
  function prepare_code_lomitko($name) {
  $good_name=$name;
  
  $trans = array(chr(195).chr(161) => "a", chr(196).chr(141) => "c", chr(196).chr(143) => "d", chr(195).chr(169) => "e", chr(196).chr(155) => "e", chr(195).chr(173) => "i", chr(197).chr(136) => "n", chr(195).chr(179) => "o", chr(197).chr(153) => "r", chr(197).chr(161) => "s", chr(197).chr(165) => "t", chr(197).chr(175) => "u", chr(195).chr(186) => "u", chr(195).chr(189) => "y", chr(197).chr(190) => "z", chr(195).chr(129) => "A", chr(196).chr(140) => "C", chr(196).chr(142) => "D", chr(195).chr(137) => "E", chr(196).chr(154) => "E", chr(195).chr(141) => "I", chr(197).chr(135) => "N", chr(195).chr(147) => "O", chr(197).chr(152) => "R", chr(197).chr(160) => "S", chr(197).chr(164) => "T", chr(195).chr(154) => "U", chr(197).chr(174) => "U", chr(195).chr(157) => "Y", chr(197).chr(189) => "Z", " " => "-", "," => "-","/" => "-", "?" => "-", "&" => "-");
  
  $good_name = strtr($good_name, $trans);

  $j=0; 
  while (substr_count($good_name, "--") > 0) {
    for ($i=0; $i<(strlen($good_name)-1); $i++) {
      if (($good_name[$i] == "-") and ($good_name[$i+1] == "-")) {
        $good_name = substr($good_name, 0, $i) . substr($good_name, ($i+1), strlen($good_name));
      }
    }
  }
  $good_name = trim($good_name, "-");
  
  return $good_name;
}

?>
