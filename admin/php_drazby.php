<?php
$editing=0;
$p_novy=0;

if (isset($_POST['novy_x'])) {
  $editing=1;
  $p_novy=1;
}

if (isset($_GET['id_drazby'])) {
  $editing=1;
  $p_novy=0;
  $p_id = $_GET['id_drazby'];
  $sql_edit = "SELECT * FROM drazby WHERE id=$p_id";
  $result_edit = mysql_query($sql_edit, $link)
    or die(mysql_error($link));
  $row_edit = mysql_fetch_array($result_edit);
  $p_nadpis = $row_edit['nadpis'];
  $p_kratky_text = stripslashes(ereg_replace('<br />', chr (13), $row_edit['kratky_text']));
  $p_dlouhy_text = stripslashes($row_edit['dlouhy_text']);
  $p_datum_a_cas = substr($row_edit['datum_a_cas'],8 , 2) . "." . substr($row_edit['datum_a_cas'],5 , 2) . "." . substr($row_edit['datum_a_cas'],0 , 4) . substr($row_edit['datum_a_cas'],10 , 6);
  $p_ocena = $row_edit['odhadovana_cena'];
  $p_vcena = $row_edit['vyvolavaci_cena'];
  $p_zruseno = $row_edit['stav'];
  $p_cancel = $row_edit['cancel'];
  $p_obrazek = $row_edit['id_obrazek'];
}

if (isset($_POST['smazat'])) {
  if ($_POST['smazat'] == "Vymazat") {
    if (isset($_POST['id_drazby'])) {
      $p_id = $_POST['id_drazby'];
      $sql_update="UPDATE drazby SET cancel=1 WHERE id=" . $p_id;
      $result_update = mysql_query($sql_update, $link)
        or die(mysql_error($link));
    }
  }
}

if (isset($_POST['obnovit'])) {
  if ($_POST['obnovit'] == "Obnovit") {
    if (isset($_POST['id_drazby'])) {
      $p_id = $_POST['id_drazby'];
      $sql_update="UPDATE drazby SET cancel=0 WHERE id=" . $p_id;
      $result_update = mysql_query($sql_update, $link)
        or die(mysql_error($link));
    }
  }
}

if (isset($_POST['smaz_posudek'])) {
  if ($_POST['smaz_posudek'] == "Smaž posudek") {
    if (isset($_POST['posudek_cesta'])) {
      if (file_exists($_POST['posudek_cesta'])){ unlink($_POST['posudek_cesta']);}
      $editing=1;
      $p_id=$_POST['id_drazby'];
      $sql_edit = "SELECT * FROM drazby WHERE id=$p_id";
      $result_edit = mysql_query($sql_edit, $link)
        or die(mysql_error($link));
      $row_edit = mysql_fetch_array($result_edit);
      $p_nadpis = $row_edit['nadpis'];
      $p_kratky_text = ereg_replace('<br />', chr (13), $row_edit['kratky_text']);
      $p_dlouhy_text = $row_edit['dlouhy_text'];
      $p_datum_a_cas = substr($row_edit['datum_a_cas'],8 , 2) . "." . substr($row_edit['datum_a_cas'],5 , 2) . "." . substr($row_edit['datum_a_cas'],0 , 4) . substr($row_edit['datum_a_cas'],10 , 6);
      $p_ocena = $row_edit['odhadovana_cena'];
      $p_vcena = $row_edit['vyvolavaci_cena'];
      $p_zruseno = $row_edit['stav'];
      $p_cancel = $row_edit['cancel'];
      $p_obrazek = $row_edit['id_obrazek'];
    }
  }
}

if (isset($_POST['smaz_vyhlasku'])) {
  if ($_POST['smaz_vyhlasku'] == "Smaž vyhlášku") {
    if (isset($_POST['vyhlaska_cesta'])) {
      if (file_exists($_POST['vyhlaska_cesta'])){ unlink($_POST['vyhlaska_cesta']);}
      $editing=1;
      $p_id=$_POST['id_drazby'];
      $sql_edit = "SELECT * FROM drazby WHERE id=$p_id";
      $result_edit = mysql_query($sql_edit, $link)
        or die(mysql_error($link));
      $row_edit = mysql_fetch_array($result_edit);
      $p_nadpis = $row_edit['nadpis'];
      $p_kratky_text = ereg_replace('<br />', chr (13), $row_edit['kratky_text']);
      $p_dlouhy_text = $row_edit['dlouhy_text'];
      $p_datum_a_cas = substr($row_edit['datum_a_cas'],8 , 2) . "." . substr($row_edit['datum_a_cas'],5 , 2) . "." . substr($row_edit['datum_a_cas'],0 , 4) . substr($row_edit['datum_a_cas'],10 , 6);
      $p_ocena = $row_edit['odhadovana_cena'];
      $p_vcena = $row_edit['vyvolavaci_cena'];
      $p_zruseno = $row_edit['stav'];
      $p_cancel = $row_edit['cancel'];
      $p_obrazek = $row_edit['id_obrazek'];
    }
  }
}

if (isset($_POST['smaz_usneseni'])) {
  if ($_POST['smaz_usneseni'] == "Smaž usnesení") {
    if (isset($_POST['usneseni_cesta'])) {
      if (file_exists($_POST['usneseni_cesta'])){ unlink($_POST['usneseni_cesta']);}
      $editing=1;
      $p_id=$_POST['id_drazby'];
      $sql_edit = "SELECT * FROM drazby WHERE id=$p_id";
      $result_edit = mysql_query($sql_edit, $link)
        or die(mysql_error($link));
      $row_edit = mysql_fetch_array($result_edit);
      $p_nadpis = $row_edit['nadpis'];
      $p_kratky_text = ereg_replace('<br />', chr (13), $row_edit['kratky_text']);
      $p_dlouhy_text = $row_edit['dlouhy_text'];
      $p_datum_a_cas = substr($row_edit['datum_a_cas'],8 , 2) . "." . substr($row_edit['datum_a_cas'],5 , 2) . "." . substr($row_edit['datum_a_cas'],0 , 4) . substr($row_edit['datum_a_cas'],10 , 6);
      $p_ocena = $row_edit['odhadovana_cena'];
      $p_vcena = $row_edit['vyvolavaci_cena'];
      $p_zruseno = $row_edit['stav'];
      $p_cancel = $row_edit['cancel'];
      $p_obrazek = $row_edit['id_obrazek'];
    }
  }
}

if (isset($_POST['smaz_oznameni'])) {
    if ($_POST['smaz_oznameni'] == "Smaž oznámení") {
        if (isset($_POST['oznameni_cesta'])) {
            if (file_exists($_POST['oznameni_cesta'])){ unlink($_POST['oznameni_cesta']);}
            $editing=1;
            $p_id=$_POST['id_drazby'];
            $sql_edit = "SELECT * FROM drazby WHERE id=$p_id";
            $result_edit = mysql_query($sql_edit, $link)
            or die(mysql_error($link));
            $row_edit = mysql_fetch_array($result_edit);
            $p_nadpis = $row_edit['nadpis'];
            $p_kratky_text = ereg_replace('<br />', chr (13), $row_edit['kratky_text']);
            $p_dlouhy_text = $row_edit['dlouhy_text'];
            $p_datum_a_cas = substr($row_edit['datum_a_cas'],8 , 2) . "." . substr($row_edit['datum_a_cas'],5 , 2) . "." . substr($row_edit['datum_a_cas'],0 , 4) . substr($row_edit['datum_a_cas'],10 , 6);
            $p_ocena = $row_edit['odhadovana_cena'];
            $p_vcena = $row_edit['vyvolavaci_cena'];
            $p_zruseno = $row_edit['stav'];
            $p_cancel = $row_edit['cancel'];
            $p_obrazek = $row_edit['id_obrazek'];
        }
    }
}

if (isset($_POST['ulozit'])) {
  if ($_POST['ulozit'] == "Uložit") {
    if (isset($_POST['je_novy'])) {
      if ($_POST['je_novy'] == "1") {
        if (isset($_POST['zrusena'])) {
          $p_zrusena = 1;
        } else { $p_zrusena = 0; }
        $p_nadpis = addslashes($_POST['nadpis']);
        $p_datum = substr($_POST['datum'],6 , 4) . "-" . substr($_POST['datum'],3 , 2) . "-" . substr($_POST['datum'],0 , 2) . substr($_POST['datum'],10 , 6);
        $p_ocena = $_POST['ocena'];
        $p_vcena = $_POST['vcena'];
        $p_kratky_text = addslashes(ereg_replace(chr (13), '<br />' , $_POST['kratky_text']));
        $p_dlouhy_text = addslashes($_POST['dlouhy_text']);

        $sql_insert_new="INSERT INTO drazby SET nadpis='" . $p_nadpis . "', kratky_text='" . $p_kratky_text . "', dlouhy_text='" . $p_dlouhy_text . "', datum_a_cas='" . $p_datum . "', odhadovana_cena='" . $p_ocena . "', vyvolavaci_cena='" . $p_vcena . "', stav='" . $p_zrusena . "'";
        $result_insert_new = mysql_query($sql_insert_new, $link)
          or die(mysql_error($link));

        $last_id = mysql_insert_id();

      if ($_FILES['vposudek']['name'] != ""){
        $p_image_upload_error = 0;
        $blacklist = array(".php", ".phtml", ".php3", ".php4");
        foreach ($blacklist as $item) {
          if(preg_match("/$item\$/i", $_FILES['vposudek']['name'])) {
            $p_image_upload_error = 3;
          }
        }

        $posudek_suffix = substr($_FILES['vposudek']['name'], -4);
        switch ($posudek_suffix) {
          case ".doc":
          case ".DOC":
          case ".pdf":
          case ".PDF":
            break;
          default:
            $p_image_upload_error = 2;
            break;
        }

        if ($p_image_upload_error == 0) {
          $uploadfile = $uploadposudek . basename($_FILES['vposudek']['name']);
          if (move_uploaded_file($_FILES['vposudek']['tmp_name'], $uploadfile)) {
            $newfilename = $uploadposudek . "posudek_" . $last_id . substr($_FILES['vposudek']['name'], -4);
            if (file_exists($newfilename)){
              unlink($newfilename);
            }
            rename($uploadfile, $newfilename);
          }
        }
      }

      if ($_FILES['vvyhlaska']['name'] != ""){
        $p_image_upload_error = 0;
        $blacklist = array(".php", ".phtml", ".php3", ".php4");
        foreach ($blacklist as $item) {
          if(preg_match("/$item\$/i", $_FILES['vvyhlaska']['name'])) {
            $p_image_upload_error = 3;
          }
        }

        $vyhlaska_suffix = substr($_FILES['vvyhlaska']['name'], -4);
        switch ($vyhlaska_suffix) {
          case ".doc":
          case ".DOC":
          case ".pdf":
          case ".PDF":
            break;
          default:
            $p_image_upload_error = 2;
            break;
        }

        if ($p_image_upload_error == 0) {
          $uploadfile = $uploadvyhlaska . basename($_FILES['vvyhlaska']['name']);
          if (move_uploaded_file($_FILES['vvyhlaska']['tmp_name'], $uploadfile)) {
            $newfilename = $uploadvyhlaska . "vyhlaska_" . $last_id . substr($_FILES['vvyhlaska']['name'], -4);
            if (file_exists($newfilename)){
              unlink($newfilename);
            }
            rename($uploadfile, $newfilename);
          }
        }
      }

      if ($_FILES['vpriklep']['name'] != ""){
        $p_image_upload_error = 0;
        $blacklist = array(".php", ".phtml", ".php3", ".php4");
        foreach ($blacklist as $item) {
          if(preg_match("/$item\$/i", $_FILES['vpriklep']['name'])) {
            $p_image_upload_error = 3;
          }
        }

        $priklep_suffix = substr($_FILES['vpriklep']['name'], -4);
        switch ($priklep_suffix) {
          case ".doc":
          case ".DOC":
          case ".pdf":
          case ".PDF":
            break;
          default:
            $p_image_upload_error = 2;
            break;
        }

        if ($p_image_upload_error == 0) {
          $uploadfile = $uploadusneseni . basename($_FILES['vpriklep']['name']);
          if (move_uploaded_file($_FILES['vpriklep']['tmp_name'], $uploadfile)) {
            $newfilename = $uploadusneseni . "usneseni_" . $last_id . substr($_FILES['vpriklep']['name'], -4);
            if (file_exists($newfilename)){
              unlink($newfilename);
            }
            rename($uploadfile, $newfilename);
          }
        }
      }

      if ($_FILES['voznameni']['name'] != ""){
          $p_image_upload_error = 0;
          $blacklist = array(".php", ".phtml", ".php3", ".php4");
          foreach ($blacklist as $item) {
              if(preg_match("/$item\$/i", $_FILES['voznameni']['name'])) {
                  $p_image_upload_error = 3;
              }
          }

          $oznameni_suffix = substr($_FILES['voznameni']['name'], -4);
          switch ($oznameni_suffix) {
              case ".doc":
              case ".DOC":
              case ".pdf":
              case ".PDF":
                  break;
              default:
                  $p_image_upload_error = 2;
                  break;
          }

          if ($p_image_upload_error == 0) {
              $uploadfile = $uploadoznameni . basename($_FILES['voznameni']['name']);
              if (move_uploaded_file($_FILES['voznameni']['tmp_name'], $uploadfile)) {
                  $newfilename = $uploadoznameni . "oznameni_" . $last_id . substr($_FILES['voznameni']['name'], -4);
                  if (file_exists($newfilename)){
                      unlink($newfilename);
                  }
                  rename($uploadfile, $newfilename);
              }
          }
      }

      } elseif(($_POST['je_novy'] == "0") and (isset($_POST['id_drazby']))) {
        $p_id = $_POST['id_drazby'];
        if (isset($_POST['zrusena'])) {
          $p_zrusena = 1;
        } else { $p_zrusena = 0; }
        $p_nadpis = addslashes($_POST['nadpis']);
        $p_datum = substr($_POST['datum'],6 , 4) . "-" . substr($_POST['datum'],3 , 2) . "-" . substr($_POST['datum'],0 , 2) . substr($_POST['datum'],10 , 6);
        $p_ocena = $_POST['ocena'];
        $p_vcena = $_POST['vcena'];
        $p_kratky_text = addslashes(ereg_replace(chr (13), '<br />' , $_POST['kratky_text']));
        $p_dlouhy_text = addslashes($_POST['dlouhy_text']);

        $sql_update="UPDATE drazby SET nadpis='" . $p_nadpis . "', kratky_text='" . $p_kratky_text . "', dlouhy_text='" . $p_dlouhy_text . "', datum_a_cas='" . $p_datum . "', odhadovana_cena='" . $p_ocena . "', vyvolavaci_cena='" . $p_vcena . "', stav='" . $p_zrusena . "' WHERE id=" . $p_id;
        $result_update = mysql_query($sql_update, $link)
          or die(mysql_error($link));


        if ($_FILES['vposudek']['name'] != ""){
          $p_image_upload_error = 0;
          $blacklist = array(".php", ".phtml", ".php3", ".php4");
          foreach ($blacklist as $item) {
            if(preg_match("/$item\$/i", $_FILES['vposudek']['name'])) {
              $p_image_upload_error = 3;
            }
          }

          $posudek_suffix = substr($_FILES['vposudek']['name'], -4);
          switch ($posudek_suffix) {
            case ".doc":
            case ".DOC":
            case ".pdf":
            case ".PDF":
              break;
            default:
              $p_image_upload_error = 2;
              break;
          }

          if ($p_image_upload_error == 0) {
            $uploadfile = $uploadposudek . basename($_FILES['vposudek']['name']);
            if (move_uploaded_file($_FILES['vposudek']['tmp_name'], $uploadfile)) {
              $newfilename = $uploadposudek . "posudek_" . $p_id . substr($_FILES['vposudek']['name'], -4);
              if (file_exists($newfilename)){
                unlink($newfilename);
              }
              rename($uploadfile, $newfilename);
            }
          }
        }

        if ($_FILES['vvyhlaska']['name'] != ""){
          $p_image_upload_error = 0;
          $blacklist = array(".php", ".phtml", ".php3", ".php4");
          foreach ($blacklist as $item) {
            if(preg_match("/$item\$/i", $_FILES['vvyhlaska']['name'])) {
              $p_image_upload_error = 3;
            }
          }

          $vyhlaska_suffix = substr($_FILES['vvyhlaska']['name'], -4);
          switch ($vyhlaska_suffix) {
            case ".doc":
            case ".DOC":
            case ".pdf":
            case ".PDF":
              break;
            default:
              $p_image_upload_error = 2;
              break;
          }

          if ($p_image_upload_error == 0) {
            $uploadfile = $uploadvyhlaska . basename($_FILES['vvyhlaska']['name']);
            if (move_uploaded_file($_FILES['vvyhlaska']['tmp_name'], $uploadfile)) {
              $newfilename = $uploadvyhlaska . "vyhlaska_" . $p_id . substr($_FILES['vvyhlaska']['name'], -4);
              if (file_exists($newfilename)){
                unlink($newfilename);
              }
              rename($uploadfile, $newfilename);
            }
          }
        }

        if ($_FILES['vpriklep']['name'] != ""){
          $p_image_upload_error = 0;
          $blacklist = array(".php", ".phtml", ".php3", ".php4");
          foreach ($blacklist as $item) {
            if(preg_match("/$item\$/i", $_FILES['vpriklep']['name'])) {
              $p_image_upload_error = 3;
            }
          }

          $priklep_suffix = substr($_FILES['vpriklep']['name'], -4);
          switch ($priklep_suffix) {
            case ".doc":
            case ".DOC":
            case ".pdf":
            case ".PDF":
              break;
            default:
              $p_image_upload_error = 2;
              break;
          }

          if ($p_image_upload_error == 0) {
            $uploadfile = $uploadusneseni . basename($_FILES['vpriklep']['name']);
            if (move_uploaded_file($_FILES['vpriklep']['tmp_name'], $uploadfile)) {
              $newfilename = $uploadusneseni . "usneseni_" . $p_id . substr($_FILES['vpriklep']['name'], -4);
              if (file_exists($newfilename)){
                unlink($newfilename);
              }
              rename($uploadfile, $newfilename);
            }
          }
        }

        if ($_FILES['voznameni']['name'] != ""){
            $p_image_upload_error = 0;
            $blacklist = array(".php", ".phtml", ".php3", ".php4");
            foreach ($blacklist as $item) {
                if(preg_match("/$item\$/i", $_FILES['voznameni']['name'])) {
                    $p_image_upload_error = 3;
                }
            }

            $oznameni_suffix = substr($_FILES['voznameni']['name'], -4);
            switch ($oznameni_suffix) {
                case ".doc":
                case ".DOC":
                case ".pdf":
                case ".PDF":
                    break;
                default:
                    $p_image_upload_error = 2;
                    break;
            }

            if ($p_image_upload_error == 0) {
                $uploadfile = $uploadoznameni . basename($_FILES['voznameni']['name']);
                if (move_uploaded_file($_FILES['voznameni']['tmp_name'], $uploadfile)) {
                    $newfilename = $uploadoznameni . "oznameni_" . $p_id . substr($_FILES['voznameni']['name'], -4);
                    if (file_exists($newfilename)){
                        unlink($newfilename);
                    }
                    rename($uploadfile, $newfilename);
                }
            }
        }

      }
    }
  }
}

if ((isset($_GET['order'])) AND (isset($_GET['how']))) {
  $_SESSION['s_order'] = $_GET['order'];
  $_SESSION['s_how'] = $_GET['how'];
}
if (!(isset($_SESSION['s_order']))) {
    $sql_sel_ordering = "SELECT * FROM drazby_filtr WHERE id=1";
    $result_sel_ordering = mysql_query($sql_sel_ordering, $link);
    $row_sel_ordering = mysql_fetch_array($result_sel_ordering);

    $_SESSION['s_nadpis'] = $row_sel_ordering['nadpis'];
    $_SESSION['s_odkdy'] = $row_sel_ordering['odkdy'];
    $_SESSION['s_dokdy'] = $row_sel_ordering['dokdy'];
    $_SESSION['s_ocena'] = $row_sel_ordering['odhadovana_cena'];
    $_SESSION['s_vcena'] = $row_sel_ordering['vyvolavaci_cena'];
    $_SESSION['s_is_valid'] = $row_sel_ordering['stav'];
    $_SESSION['s_is_deleted'] = $row_sel_ordering['cancel'];
    $_SESSION['s_order'] = $row_sel_ordering['what_order'];
    $_SESSION['s_how'] = $row_sel_ordering['how_order'];
}

  if ((isset($_POST['reset_x'])) || (isset($_POST['uloz_x'])) || (isset($_POST['input_filtr']))){

    if ($_POST['input_filtr'] == "filtruj") {
      $_SESSION['s_nadpis'] = $_POST['nadpis'];
      $_SESSION['s_odkdy'] = $_POST['odkdy'];
      $_SESSION['s_dokdy'] = $_POST['dokdy'];
      $_SESSION['s_ocena'] = $_POST['ocena'];
      $_SESSION['s_vcena'] = $_POST['vcena'];
      $_SESSION['s_is_valid'] = $_POST['is_valid'];
      $_SESSION['s_is_deleted'] = $_POST['is_deleted'];
    }

    if (isset($_POST['reset_x'])) {
      $_SESSION['s_nadpis'] = "";
      $_SESSION['s_odkdy'] = "";
      $_SESSION['s_dokdy'] = "";
      $_SESSION['s_ocena'] = "";
      $_SESSION['s_vcena'] = "";
      $_SESSION['s_is_valid'] = "aktivní";
      $_SESSION['s_is_deleted'] = "aktivní";
      $_SESSION['s_order'] = "datum_a_cas";
      $_SESSION['s_how'] = "vzestupně";
    }

    if (isset($_POST['uloz_x'])) {
      $_SESSION['s_nadpis'] = $_POST['nadpis'];
      $_SESSION['s_odkdy'] = $_POST['odkdy'];
      $_SESSION['s_dokdy'] = $_POST['dokdy'];
      $_SESSION['s_ocena'] = $_POST['ocena'];
      $_SESSION['s_vcena'] = $_POST['vcena'];
      $_SESSION['s_is_valid'] = $_POST['is_valid'];
      $_SESSION['s_is_deleted'] = $_POST['is_deleted'];

      $sql_filtering = "UPDATE drazby_filtr SET nadpis='" . ($_SESSION['s_nadpis']) . "', odkdy='" . ($_SESSION['s_odkdy']) . "', dokdy='" . ($_SESSION['s_dokdy']) . "', odhadovana_cena='" . ($_SESSION['s_ocena']) . "', vyvolavaci_cena='" . ($_SESSION['s_vcena']) . "', stav='" . ($_SESSION['s_is_valid']) . "', cancel='" . ($_SESSION['s_is_deleted']) . "', what_order='" . ($_SESSION['s_order']) . "', how_order='" . ($_SESSION['s_how']) . "' WHERE id=1";
      $result_filtering = mysql_query($sql_filtering, $link);
      if (!$result_filtering) { echo mysql_error($link);}

    }
  }
?>


