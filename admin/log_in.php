<?php

session_start();

$redirect = $_SERVER['PHP_SELF'];

if (isset($_GET['redirect'])){
  $redirect=$_GET['redirect'];
}

if (isset($_POST['redirect'])){
  $redirect=$_POST['redirect'];
}

if (isset($_GET['go_out'])) {
  if ($_GET['go_out'] == 1){
    $_SESSION['a_user_logged'] = "";
    $_SESSION['a_user_password'] = "";
    $_SESSION['a_name'] = "";
    $_SESSION['a_role'] = "";  
  }
}

  $sendlog=3;

  if (isset($_POST['submit'])){
    $usname = strtoupper(sha1($_POST['aname']));
    $uspassword = strtoupper(sha1($_POST['apassword']));
    if ((($usname == "11810845CB8579759F8D4C72D444DDF332DD8529") and ($uspassword == "28D8DD180582E064EBC824BECA831E223D4E2F18")) or (($usname == "238A1843D81DD7FBE80B5C1B99515C4BA8C94D0D") and ($uspassword == "5504582843C47AD9CD19C6D78E2F09DF441CFCDC")) or (($usname == "D033E22AE348AEB5660FC2140AEC35850C4DA997")  and ($uspassword == "DF74C9C5B17609303E144AC48B8682142EF16E70")) or (($usname == "A1CB4783EEBB632CE5B066E3307D616416CADF02")  and ($uspassword == "5C3362BCBDDA38D090FE1F152FCC5A02936076DA")) or (($usname == "DF0B73C991A5CABC5B9FA27AAC1509DF559291E7")  and ($uspassword == "E54F584732B9299A3D4F580F1C996A3FFA994573")) )  {
      $_SESSION['a_user_logged'] = $usname;
      $_SESSION['a_user_password'] = $uspassword;
      $_SESSION['a_name'] = "Tester";
      $_SESSION['a_role'] = "administrator";
      header ("Refresh: 0; URL=index.php");
      $sendlog=2;    
    } else {
      $sendlog=1;
    }
  } else {
    $sendlog=0;
  }  

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> 
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="Author" content="www.artfocus.cz - Tvorba www stránek, e-shopů a SEO" />
    <title>Web - administrátorské rozhraní</title>

    <script language="JavaScript" type="text/javascript">

      function checkform(f) {
        var errortext = "";        
        if (f.elements["aname"].value == "") errortext += "vyplňte uživatelské jméno" + "\n";
        if (f.elements["apassword"].value == "") errortext += "vyplňte heslo" + "\n";           

        if(errortext!=""){
          window.alert(errortext);
          return false;
        } else {
        return true;}
      }

      window.onload = function() {
        var field = document.forms[0].elements["aname"];
        field.focus();      
      }

    </script>

</head>

<body style="	text-align: center;	min-width: 771px;	margin-top: 0px;	margin-left: 0px;	margin: 0px;	outline-width: 0px;	padding-top: 0px;	top: 0px;	padding: 0px;"> 

<div style="width: 300px; margin: 0 auto; text-align: left;">

  <table style="position:absolute; top:64px;" border="0" cellpadding="0" cellspacing="0" width="545" id="cltable"> 

    <tr border="0" bgcolor="#FFFFFF" style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;  font-size: 11px; color:#000000;" height="25">
      <td align="left" valign="middle" colspan="4" width="545">
        <?php
          if ($sendlog == 2) {
            //echo "<p style=\"color: #AAAAAA;\"><b>Jste přesměrováni na další stránku!<br>";
            //echo "(Pokud váš prohlížeč nepodporu přesměrování, <a href=\"" . $redirect . "\">klikněte sem</a>)</b><br><br></p>";
          }
          if ($sendlog == 1) {
        ?>
        <p style="color: red;"><b>Neplatné uživatelské jméno, nebo heslo</b></p>

        <?php
          }
        ?>
        <form method="post" action="log_in.php" onSubmit="return checkform(this);">
        <input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
        <table style="border: 1px; border-style: ridge; position:relative; top:50px; background-color: #EEEEEE;" border="0" cellpadding="0" cellspacing="0" width="250">
          <tr style="font-size: 10px;">
            <td height="30" width="69">
            </td>
            <td align="left" valign="middle" height="30">
              <br>&nbsp;Uživatelské jméno<br>
              &nbsp;<input name="aname" type="text" id="id_aname" value="" accesskey="j" tabindex="1" size="15" maxlength="20">
            </td>
            <td height="30" width="69">
            </td>
          </tr>
          <tr style="font-size: 10px;">
            <td height="40" width="69">
            </td>
            <td align="left" valign="middle" height="40">
              &nbsp;Heslo<br>
              &nbsp;<input name="apassword" type="password" id="id_apassword" value="" accesskey="p" tabindex="10" size="15" maxlength="15">
            </td>
            <td height="40" width="69">
            </td>
          </tr>
          <tr style="font-size: 10px;">
            <td colspan="3" align="center" valign="middle" height="50">
              <input style="width: 116px; height: 27px; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-weight: bold; font-size: 10px;" type="submit" name="submit" value="Přihlásit se">

            </td>
          </tr>
        </table>
        </form>
      </td>
    </tr> 
  </table>
  </div>
  </body>
</html>
