<?php
  require('connect.php');
  
  if (!(isset($_GET['id']))) {    
    header("Location: drazby.html");
    die;
  }
  $id_detail = $_GET['id'];
    
  $sql_drazby = "SELECT * FROM drazby WHERE id='$id_detail' AND cancel='0'";
  $result_drazby = mysql_query($sql_drazby)
    or die("Invalid query: " . mysql_error($result_drazby));
  $row_drazby = mysql_fetch_array($result_drazby);

  $p_nadpis = stripslashes($row_drazby['nadpis']);
  $p_kratky_text = stripslashes($row_drazby['kratky_text']);
  $p_dlouhy_text = stripslashes($row_drazby['dlouhy_text']);
  $p_datum_a_cas = substr($row_drazby['datum_a_cas'],8 , 2) . "." . substr($row_drazby['datum_a_cas'],5 , 2) . "." . substr($row_drazby['datum_a_cas'],0 , 4) . substr($row_drazby['datum_a_cas'],10 , 6);
  $p_odhadovana_cena = $row_drazby['odhadovana_cena'];
  $p_vyvolavaci_cena = $row_drazby['vyvolavaci_cena'];
  $p_m_obrazek = $uploadbig . "obrazek_" . $row_drazby['id_obrazek'] . ".jpg";
  if (!(file_exists($p_m_obrazek))){
    $p_m_obrazek = "img/bez_obrazku_big.jpg";
  }
  $p_m_obrazek_s = $uploadsmall . "obrazek_" . $row_drazby['id_obrazek'] . ".jpg";
  if (!(file_exists($p_m_obrazek_s))){
    $p_m_obrazek_s = "img/bez_obrazku_small.jpg";
  }  
  $p_m_obrazek_b = $uploadoriginal . "obrazek_" . $row_drazby['id_obrazek'] . ".jpg";
  if (!(file_exists($p_m_obrazek))){
    $p_m_obrazek = "img/bez_obrazku_big.jpg";
  }  
  $p_stav = $row_drazby['stav']; 
  $p_selected_picture = $row_drazby['id_obrazek'];
  
  $p_odkaz_vyhlaska = $uploadvyhlaska . "vyhlaska_" . $id_detail;
  if (file_exists($p_odkaz_vyhlaska . ".doc")) {
    $p_odkaz_vyhlaska = $p_odkaz_vyhlaska . ".doc";
  } elseif (file_exists($p_odkaz_vyhlaska . ".pdf")) {
    $p_odkaz_vyhlaska = $p_odkaz_vyhlaska . ".pdf";
  }
  
  $p_odkaz_posudek = $uploadposudek . "posudek_" . $id_detail;
  if (file_exists($p_odkaz_posudek . ".doc")) {
    $p_odkaz_posudek = $p_odkaz_posudek . ".doc";
  } elseif (file_exists($p_odkaz_posudek . ".pdf")) {
    $p_odkaz_posudek = $p_odkaz_posudek . ".pdf";
  }
  
  $p_odkaz_usneseni = $uploadusneseni . "usneseni_" . $id_detail;
  if (file_exists($p_odkaz_usneseni . ".doc")) {
    $p_odkaz_usneseni = $p_odkaz_usneseni . ".doc";
  } elseif (file_exists($p_odkaz_usneseni . ".pdf")) {
    $p_odkaz_usneseni = $p_odkaz_usneseni . ".pdf";
  }
  
  $sql_obrazky = "SELECT * FROM obrazky WHERE id_drazby='$id_detail'";
  $result_obrazky = mysql_query($sql_obrazky)
    or die("Invalid query: " . mysql_error($result_obrazky));
  
  $i=0;  
  $obr_array = array();  
  while ($row_obrazky = mysql_fetch_array($result_obrazky)) {    
      $obr_array[$i]['velky_obrazek']=$uploadoriginal . "obrazek_" . $row_obrazky['id'] . ".jpg";
      $obr_array[$i]['maly_obrazek']=$uploadsmall . "obrazek_" . $row_obrazky['id'] . ".jpg";
      $obr_array[$i]['id']=$row_obrazky['id'];
      $i++;    
  }

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta name="keywords" content="<?php echo $p_nadpis; ?> | Exekutorský Úřad Praha 8" />
<meta name="description" content="<?php echo $p_nadpis; ?> | Exekutorský Úřad Praha 8" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta name="robots" content="index,follow" />
<link href="styles/exekutor.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />
<link rel="icon" href="exekutor.ico" type="image/x-icon" />
<title><?php echo $p_nadpis; ?> | Exekutorský Úřad Praha 8</title>
<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/scriptaculous.js?load=effects,builder"></script>
<script type="text/javascript" src="js/lightbox.js"></script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
</head>
<body>
<div style="display:none;">  <img src="img/menu1_over.jpg" alt="" />
  <img src="img/menu2_over.jpg" alt="" />
  <img src="img/menu3_over.jpg" alt="" />
  <img src="img/menu4_over.jpg" alt="" />
  <img src="img/menu5_over.jpg" alt="" />
  <img src="img/menu6_over.jpg" alt="" />
  <img src="img/menu7_over.jpg" alt="" />
  <img src="img/submenu1_over.jpg" alt="" />
</div>
	<div id="back_center"><!--zacentrovane pozadi-->
		<a title="Exekutorský úřad Praha 8" href="index.html" id="odkaz_main"><span id="baner_main"></span></a><!--hlavni baner-->
		<div id="menu_main"><!--hlavni menu-->
			<ul class="tabs">
				<li><a href="soukrome-exekuce.html" class="m1"><em>základní informace</em></a></li>
				<li><a href="pro-dluzniky.html" class="m2"><em>pro dlužníky</em></a></li>
				<li><a href="pro-zamestnavatele.html" class="m3"><em>pro zaměstnavatele</em></a></li>
				<li><a href="exekucni-cinnost.html" class="m4"><em>exekuční činnost</em></a></li>
				<li><a href="exekutorske-zapisy.html" class="m5"><em>exekutorské zápisy</em></a></li>
				<li><a href="drazby.html" class="n6"><em>dražby</em></a></li>
				<li><a href="kontakt.html" class="m7"><em>kontakt</em></a></li>
			</ul>
		</div>
		<div id="submenu">
			<div id="submenu_in">
				<ul class="tabs2">
				<li><a href="drazby.html" class="m"><em>přehled dražeb</em></a></li>
				<li><a href="informace-pro-zajemce.html" class="m"><em>informace pro zájemce</em></a></li>
				</ul>
			</div>
		</div>
		<div id="back_content1">
			<div id="content_top" style="background:url(img/left_blue_drazby.jpg) no-repeat; ">	
			  <h1 id="description" style="font-size: 16px; font-weight: bolder;"><?php  echo stripslashes($p_nadpis); ?></h1>
				<div id="content_top_left">
					<img src="img/drazby.jpg" alt="Přehled dražeb" />
				</div>				
			</div>
			<div id="content_detail">			
			<?php 
            $fileExists = 0;
            if (file_exists($p_odkaz_posudek)) $fileExists++;
            if (file_exists($p_odkaz_vyhlaska)) $fileExists++;
            if (file_exists($p_odkaz_usneseni)) $fileExists++;
            $rownumber = 5 + $fileExists;
            
            $framedelete    = "";
            $framedelete_p  = "";
            $framedelete_v  = "";
            $noBottomBorder = " style='border-bottom: 0px;' ";
            
            if (!file_exists($p_odkaz_usneseni)) {
                if (file_exists($p_odkaz_vyhlaska)) {
                    $framedelete_v = $noBottomBorder;
                } else {
                    if (file_exists($p_odkaz_posudek)) {
                        $framedelete_p = $noBottomBorder;
                    } else {
                        $framedelete = $noBottomBorder;
                    }
                }
            }

          echo "          
          <table cellpadding=\"2\" cellspacing=\"0\" id=\"main_table_detail\">
            <tr>
              <td colspan=\"2\" id=\"td_picture_detail\">";
              if ($p_selected_picture != 0) {
                echo "<a href=\"$p_m_obrazek_b\" rel=\"lightbox\" title=\"$p_nadpis\">";
              }
              echo "<img src=\"$p_m_obrazek\" title=\"$p_nadpis\" alt=\"$p_nadpis\" class=\"list_picture\" />";
              if ($p_selected_picture != 0) {
                echo "</a>";
              }              
              echo"</td>
              <td rowspan=\"$rownumber\" align=\"left\" valign=\"top\" style=\"width: 560px; text-align: left;\">
                <div style=\"width: 554px; overflow: hidden;\">
                  <div id=\"wysiwygd\">
                    $p_dlouhy_text
                  </div>
                </div>
              </td>              
            </tr>
            <tr>
              <td colspan=\"2\" valign=\"top\" id=\"table_over_table_detail\">
                <table cellpadding=\"0\" cellspacing=\"0\" style=\"position: relative; border: 0px; margin: 0px;\">
                  <tr>";                    
                    if (count($obr_array)>0) {
                      for ($i=0; (($i<4) and ($i<count($obr_array))); $i++) {
                        $j=$i+1;
                        echo "<td><a href=\"{$obr_array[$i]['velky_obrazek']}\" rel=\"lightbox[$p_nadpis]\" title=\"$p_nadpis obrázek č. $j\"><img src=\"{$obr_array[$i]['maly_obrazek']}\" title=\"$p_nadpis obrázek č. $j\" alt=\"$p_nadpis obrázek č. $j\" class=\"list_picture_small\" /></a></td>";
                      }
                    }
                    echo "
                  </tr>
                  <tr>";                    
                    if (count($obr_array)>4) {
                      for ($i=4; (($i<8) and ($i<count($obr_array))); $i++) {
                        $j=$i+1;
                        echo "<td><a href=\"{$obr_array[$i]['velky_obrazek']}\" rel=\"lightbox[$p_nadpis]\" title=\"$p_nadpis obrázek č. $j\"><img src=\"{$obr_array[$i]['maly_obrazek']}\" title=\"$p_nadpis obrázek č. $j\" alt=\"$p_nadpis obrázek č. $j\" class=\"list_picture_small\" /></a></td>";
                      }
                    }
                    echo "
                  </tr>
                  <tr>";                    
                    if (count($obr_array)>8) {
                      for ($i=8; (($i<12) and ($i<count($obr_array))); $i++) {
                        $j=$i+1;
                        echo "<td><a href=\"{$obr_array[$i]['velky_obrazek']}\" rel=\"lightbox[$p_nadpis]\" title=\"$p_nadpis obrázek č. $j\"><img src=\"{$obr_array[$i]['maly_obrazek']}\" title=\"$p_nadpis obrázek č. $j\" alt=\"$p_nadpis obrázek č. $j\" class=\"list_picture_small\" /></a></td>";
                      }
                    }
                    echo "
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td class=\"pevny_text_detail\">datum a čas
              </td>
              <td class=\"prom_text_detail\">$p_datum_a_cas</td>              
            </tr>
            <tr>
              <td class=\"pevny_text_detail\">odhadní cena
              </td>
              <td class=\"prom_text_detail\">" . number_format($p_odhadovana_cena, 0, ',', ' ') . "&nbsp;Kč</td>              
            </tr>
            <tr>
              <td class=\"pevny_text_detail\" $framedelete>nejnižší podání
              </td>
              <td class=\"prom_text_detail\" $framedelete>" . number_format($p_vyvolavaci_cena, 0, ',', ' ') . "&nbsp;Kč</td>              
            </tr>";
            if (file_exists($p_odkaz_posudek)) {
              echo "
              <tr>
                <td class=\"pevny_text_detail\" $framedelete_p>znalecký posudek
                </td>
                <td class=\"prom_text_detail\" $framedelete_p>"; 
                if (file_exists($p_odkaz_posudek)){
                  echo "<a href=\"$p_odkaz_posudek\" title=\"Stáhněte si Znalecký posudek\" target=\"_blank\">Stáhněte si</a>"; 
                }
                echo "</td>              
              </tr>";
            }
            if (file_exists($p_odkaz_vyhlaska)) {
              echo "
              <tr>
                <td class=\"pevny_text_detail\" $framedelete_v>dražební vyhláška
                </td>
                <td class=\"prom_text_detail\" $framedelete_v>"; 
                if (file_exists($p_odkaz_vyhlaska)){
                  echo "<a href=\"$p_odkaz_vyhlaska\" title=\"Stáhněte si Dražební vyhlášku\" target=\"_blank\">Stáhněte si</a>";
                } 
                echo "</td>              
              </tr>";
            }
            if (file_exists($p_odkaz_usneseni)) {
              echo "
              <tr>
                <td class=\"pevny_text_detail\" style=\"border-bottom: 0px;\">usnesení o příklepu</td>
                <td class=\"prom_text_detail\" style=\"border-bottom: 0px;\">"; 
                if (file_exists($p_odkaz_usneseni)){
                  echo "<a href=\"$p_odkaz_usneseni\" title=\"Stáhněte si Usnesení o příklepu\" target=\"_blank\">Stáhněte si</a>";
                } 
                echo "</td>              
              </tr>";
            }            
            echo "
          </table>";
        
      ?>
			</div>
		</div>
		<div id="footer1"></div>
		<div id="footer2">Copyright&copy;2008</div>
		<div id="footer3"><a href="http://www.artfocus.cz/tvorba-www-stranek/webdesign.html" target="_blank" class="copyrght" title="Tvorba www stránek - webdesign">webdesign</a>, <a href="http://www.artfocus.cz/seo-optimalizace/optimalizace-pro-vyhledavace.html" target="_blank" class="copyrght" title="SEO - optimalizace www stránek pro vyhledávače">SEO</a>: <a href="http://www.artfocus.cz" target="_blank" class="copyrght" title="Webdesign, SEO, grafické studio">ArtFocus</a></div>
	</div>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-2817892-15");
pageTracker._trackPageview();
</script>
</body>
</html>

