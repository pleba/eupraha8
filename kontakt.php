<?php
  require('connect.php');
  $kontakt = "";
  $uredni_hodiny = "";

  $sql = "SELECT kontakt, uredni_hodiny FROM obsah WHERE id = '1'";
  $result = mysql_query($sql)
  or die("Invalid query: " . mysql_error($link));
  
  $row = mysql_fetch_array($result);    
  $kontakt = stripslashes($row['kontakt']);
  $uredni_hodiny = stripslashes($row['uredni_hodiny']);

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="<?php echo strip_tags($kontakt); ?>" />
<meta name="description" content="<?php echo strip_tags($kontakt); ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta name="robots" content="index,follow" />
<link href="styles/exekutor.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="exekutor.ico" type="image/x-icon" />
<title>Kontakt | Exekutorský Úřad Praha 8</title>

<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAELy-6T0UdGihr-3-J4K5yRSO2EN1vOGxdZo1l5OgRLBIawavbxRTEyHWWAwzcVKINnW2W6TkG4EQfg"
  type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function load() {
  if (GBrowserIsCompatible()) {
    var map = new GMap2(document.getElementById("map"));
    map.setCenter(new GLatLng(50.055259, 14.501153), 16);
    map.addControl(new GLargeMapControl());
    map.addControl(new GScaleControl());
    map.addControl(new GOverviewMapControl());
    var mapControl = new GMapTypeControl();
    map.addControl(mapControl);
         
    var point = new GLatLng(50.055259, 14.501153);
    map.addOverlay(new GMarker(point));  
    map.openInfoWindowHtml(point, "<div style=\"text-align: left;\"><b>Exekutorský Úřad Praha 8<\/b><br />Střemchová 3013/16<br />106 00,  Praha 10 - Záběhlice<\/div>");
  }
}
//]]>
</script>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
</head>
<body onload="load()" onunload="GUnload()">
<div style="display:none;">  <img src="img/menu1_over.jpg" alt="" />
  <img src="img/menu2_over.jpg" alt="" />
  <img src="img/menu3_over.jpg" alt="" />
  <img src="img/menu4_over.jpg" alt="" />
  <img src="img/menu5_over.jpg" alt="" />
  <img src="img/menu6_over.jpg" alt="" />
  <img src="img/menu7_over.jpg" alt="" />
  <img src="img/submenu1_over.jpg" alt="" />
</div>
	<div id="back_center"><!--zacentrovane pozadi-->
		<a title="Exekutorský úřad Praha 8" href="index.html" id="odkaz_main"><span id="baner_main"></span></a><!--hlavni baner-->
		<div id="menu_main"><!--hlavni menu-->
			<ul class="tabs">
				<li><a href="soukrome-exekuce.html" class="m1"><em>základní informace</em></a></li>
				<li><a href="pro-dluzniky.html" class="m2"><em>pro dlužníky</em></a></li>
				<li><a href="pro-zamestnavatele.html" class="m3"><em>pro zaměstnavatele</em></a></li>
				<li><a href="exekucni-cinnost.html" class="m4"><em>exekuční činnost</em></a></li>
				<li><a href="exekutorske-zapisy.html" class="m5"><em>exekutorské zápisy</em></a></li>
				<li><a href="drazby.html" class="m6"><em>dražby</em></a></li>
				<li><a href="kontakt.html" class="n7"><em>kontakt</em></a></li>
			</ul>
		</div>
		<div id="submenu">
			<div id="submenu_in">
				<ul class="tabs2"><li>&nbsp;</li>
				</ul>
			</div>
		</div>
		<div id="back_content1">
			<div id="content_top" style="background:url(img/left_blue_kontakt.jpg) no-repeat; ">	
				<div id="content_top_left">
					<img src="img/kontakt.jpg" alt="Kontakt" />
				</div>
				<div id="content_top_right_kontakt">
          <div id="content_top_right_hodiny">
            <div id="around_text_1">
              <?php echo $uredni_hodiny; ?>
            </div>
          </div>
        </div>
				<div id="content_top_right2_kontakt">
          <h1 id="around_text_2">
            <?php echo $kontakt; ?>
          </h1>
        </div>
			</div>
			<div id="back_content_kontakt">
			  <div id="map" style="width: 700px; height: 500px; margin: 0px auto; border: inset;"></div>
			</div>
		</div>
		<div id="footer1"></div>
		<div id="footer2">Copyright&copy;2008</div>
		<div id="footer3"><a href="http://www.artfocus.cz/tvorba-www-stranek/webdesign.html" target="_blank" class="copyrght" title="Tvorba www stránek - webdesign">webdesign</a>, <a href="http://www.artfocus.cz/seo-optimalizace/optimalizace-pro-vyhledavace.html" target="_blank" class="copyrght" title="SEO - optimalizace www stránek pro vyhledávače">SEO</a>: <a href="http://www.artfocus.cz" target="_blank" class="copyrght" title="Webdesign, SEO, grafické studio">ArtFocus</a></div>
	</div>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-2817892-15");
pageTracker._trackPageview();
</script>
</body>
</html>

