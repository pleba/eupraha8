<?php


function prepare_name($name) {
  $good_name=$name;
  
  $trans = array(chr(195).chr(161) => "a", chr(196).chr(141) => "c", chr(196).chr(143) => "d", chr(195).chr(169) => "e", chr(196).chr(155) => "e", chr(195).chr(173) => "i", chr(197).chr(136) => "n", chr(195).chr(179) => "o", chr(197).chr(153) => "r", chr(197).chr(161) => "s", chr(197).chr(165) => "t", chr(197).chr(175) => "u", chr(195).chr(186) => "u", chr(195).chr(189) => "y", chr(197).chr(190) => "z", chr(195).chr(129) => "A", chr(196).chr(140) => "C", chr(196).chr(142) => "D", chr(195).chr(137) => "E", chr(196).chr(154) => "E", chr(195).chr(141) => "I", chr(197).chr(135) => "N", chr(195).chr(147) => "O", chr(197).chr(152) => "R", chr(197).chr(160) => "S", chr(197).chr(164) => "T", chr(195).chr(154) => "U", chr(197).chr(174) => "U", chr(195).chr(157) => "Y", chr(197).chr(189) => "Z", "." => "-", " " => "-", "," => "-");
  
  $good_name = strtr($good_name, $trans);  
  $good_name = strtolower($good_name); 

  $good_name = str_replace("/", "-", $good_name);
  
  $j=0;
  while (substr_count($good_name, "--") > 0) {
    for ($i=0; $i<(strlen($good_name)-1); $i++) {
      if (($good_name[$i] == "-") and ($good_name[$i+1] == "-")) {
        $good_name = substr($good_name, 0, $i) . substr($good_name, ($i+1), strlen($good_name));
      }
    }
  }
  $good_name = trim($good_name, "-");

  return $good_name;
}

function prepare_code($name) {
  $good_name=$name;
  
  $trans = array(chr(195).chr(161) => "a", chr(196).chr(141) => "c", chr(196).chr(143) => "d", chr(195).chr(169) => "e", chr(196).chr(155) => "e", chr(195).chr(173) => "i", chr(197).chr(136) => "n", chr(195).chr(179) => "o", chr(197).chr(153) => "r", chr(197).chr(161) => "s", chr(197).chr(165) => "t", chr(197).chr(175) => "u", chr(195).chr(186) => "u", chr(195).chr(189) => "y", chr(197).chr(190) => "z", chr(195).chr(129) => "A", chr(196).chr(140) => "C", chr(196).chr(142) => "D", chr(195).chr(137) => "E", chr(196).chr(154) => "E", chr(195).chr(141) => "I", chr(197).chr(135) => "N", chr(195).chr(147) => "O", chr(197).chr(152) => "R", chr(197).chr(160) => "S", chr(197).chr(164) => "T", chr(195).chr(154) => "U", chr(197).chr(174) => "U", chr(195).chr(157) => "Y", chr(197).chr(189) => "Z", "." => "-", " " => "-", "," => "-");
  
  $good_name = strtr($good_name, $trans);

  $j=0;
  while (substr_count($good_name, "--") > 0) {
    for ($i=0; $i<(strlen($good_name)-1); $i++) {
      if (($good_name[$i] == "-") and ($good_name[$i+1] == "-")) {
        $good_name = substr($good_name, 0, $i) . substr($good_name, ($i+1), strlen($good_name));
      }
    }
  }
  $good_name = trim($good_name, "-");
  
  return $good_name;
}

function string_to_big($name) {
  $good_name=$name;
  
  $trans = array(chr(195).chr(161) => chr(195).chr(129), chr(196).chr(141) => chr(196).chr(140), chr(196).chr(143) => chr(196).chr(142), chr(195).chr(169) => chr(195).chr(137), chr(196).chr(155) => chr(196).chr(154), chr(195).chr(173) => chr(195).chr(141), chr(197).chr(136) => chr(197).chr(135), chr(195).chr(179) => chr(195).chr(147), chr(197).chr(153) => chr(197).chr(152), chr(197).chr(161) => chr(197).chr(160), chr(197).chr(165) => chr(197).chr(164), chr(197).chr(175) => chr(197).chr(174), chr(195).chr(186) => chr(195).chr(154), chr(195).chr(189) => chr(195).chr(157), chr(197).chr(190) => chr(197).chr(189), "a" => "A", "b" => "B", "c" => "C", "d" => "D", "e" => "E", "f" => "F", "g" => "G", "h" => "H", "i" => "I", "j" => "J", "k" => "K", "l" => "L", "m" => "M", "n" => "N", "o" => "O", "p" => "P", "q" => "Q", "r" => "R", "s" => "S", "t" => "T", "u" => "U", "v" => "V", "w" => "W", "x" => "X", "y" => "Y", "z" => "Z");
  
  $good_name = strtr($good_name, $trans);  
  
  return $good_name;
}

function make_detail_url($id, $name) {
  $url_string = $name . "-D" . $id . ".html";
  return $url_string;
}

?>
